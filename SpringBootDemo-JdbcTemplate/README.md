# SrpingBoot中原生JdbcTemplate操作数据库

---

### 一：jdbc与连接池

```xml
<dependency>
    <groupId>org.mybatis.spring.boot</groupId>
    <artifactId>mybatis-spring-boot-starter</artifactId>
    <version>2.1.4</version>
</dependency>

// 上述依赖会间接注入org.springframework.boot:spring-boot-starter-jdbc:2.1.4
// org.springframework.boot:spring-boot-starter-jdbc:2.1.4 会间接注入 org.springframework.boot:spring-boot-starter:2.4.2
// org.springframework.boot:spring-boot-starter-jdbc:2.1.4 会间接注入 com.zaxxer:HikariCP:3.4.5
// org.springframework.boot:spring-boot-starter-jdbc:2.1.4 会间接注入 org.springframework:spring-jdbc:5.3.3
```



### 二：mysql数据库连接库

```xml
<dependency>
    <groupId>mysql</groupId>
    <artifactId>mysql-connector-java</artifactId>
    <version>8.0.23</version>
</dependency>
```



### 三：注入

```java
// spring-boot-jdbc默认数据库连接池HikariCP
private DataSource dataSource;
private JdbcTemplate jdbcTemplate;

@Autowired
public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
	this.jdbcTemplate = jdbcTemplate;
}

@Autowired
public void setDataSource(DataSource dataSource) {
	this.dataSource = dataSource;
}
```



