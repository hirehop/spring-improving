package com.xux.demo;

import com.xux.demo.entity.Student;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;

import javax.sql.DataSource;
import java.util.List;
import java.util.UUID;


@SpringBootTest
class DemoApplicationTests {

    private DataSource dataSource;
    private JdbcTemplate jdbcTemplate;

    @Autowired
    public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Autowired
    public void setDataSource(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    @Test
    void contextLoads() {
        jdbcTemplate.setDataSource(dataSource);

        for (int i = 0; i < 10; i++) {
            Student student = new Student(UUID.randomUUID().toString().replaceAll("-", ""), "ERROR", "男", 45);
            String sql1 = "INSERT INTO STUDENT VALUES(?,?,?,?)";
            int res = jdbcTemplate.update(sql1, student.getId(), student.getName(), student.getGender(), student.getAge());
            System.out.println("共" + res + "行受到影响。");
        }

        String sql = "SELECT * FROM STUDENT";
        List<Student> stuList = jdbcTemplate.query(sql, new BeanPropertyRowMapper<>(Student.class));
        stuList.forEach(System.out::println);
    }

}
