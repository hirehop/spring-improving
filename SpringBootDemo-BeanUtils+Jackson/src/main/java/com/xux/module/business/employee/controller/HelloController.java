package com.xux.module.business.employee.controller;

import com.xux.module.business.employee.domain.bo.EmployeeBO;
import com.xux.module.business.employee.domain.entity.EmployeeEntity;
import com.xux.module.business.employee.domain.vo.EmployeeVO;
import com.xux.util.IBeanUtil;
import org.joda.time.LocalDateTime;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 测试封装的BeanUtil类，其中实现基于Spring中BeanUtils
 *
 * @Create by X
 * @Date 2021/2/8 19:46
 */
@RestController
public class HelloController {

    @RequestMapping("/hello")
    public EmployeeVO helloWorld(){
        EmployeeEntity employeeEntity = new EmployeeEntity();
        employeeEntity.setId(1L);
        employeeEntity.setCreateTime(LocalDateTime.now().plusDays(-1).toDate());
        employeeEntity.setUpdateTime(LocalDateTime.now().toDate());
        employeeEntity.setName("xiaoming");
        employeeEntity.setAge(32);

        // 将employeeEntity拷贝到EmployeeBO中
        EmployeeBO employeeBO = new EmployeeBO();
        IBeanUtil.copyProperties(employeeEntity,employeeBO);
        employeeBO.setIsSuperman(true);


        // 将BO转为VO返回给前端
        EmployeeVO employeeVO = IBeanUtil.copy(employeeBO, EmployeeVO.class);

        return employeeVO;
    }
}
