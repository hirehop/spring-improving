package com.xux.module.business.employee.domain.bo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;


@Data
@NoArgsConstructor
@AllArgsConstructor
public class EmployeeBO {
    private Long id;

    private String name;

    private Integer age;

    private Date createTime;

    private Date updateTime;

    private Boolean isSuperman;
}
