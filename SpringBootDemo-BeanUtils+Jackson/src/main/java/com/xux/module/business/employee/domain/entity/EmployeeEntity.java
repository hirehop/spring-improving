package com.xux.module.business.employee.domain.entity;

import com.xux.common.domain.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class EmployeeEntity extends BaseEntity {
    private String name;
    private Integer age;
}
