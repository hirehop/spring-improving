# JDK5特性（介绍5个特性：自用）

---

## 一：JDK5自动拆装箱

- Integer + Integer : int
- Integer + int : int
- Integer + Double : double
- Integer + double : double
- Double + Double : double

1> 注意事项

- 强转只能在基本类型之间进行！
- Double可以自动拆箱为double，但是不能跨级拆为int！

```java
Double d = 45.56;
double res1 = d; // pass
// int res2 = d; // error
int res3 = (int)d.doubleValue(); // pass
```

- 对于项目中需要处理的数据，全部使用Double进行泛型约束

  - 对于部分特殊需要转换为int的数据，可以单独对其进行处理

- 大部分情况我们操作的是字符串类型数据

  ```java
  System.out.println(Double.parseDouble("45.608"));  // 45.608
  System.out.println(Double.parseDouble("45"));  // 15.0
  System.out.println(String.format("%.3f",Double.parseDouble("45")));  // 45.000
  // 强转不会四舍五入，且只能在基本类型之间转换。
  System.out.println((int) Double.parseDouble("45.608"));  // 45
  System.out.println((int) Double.parseDouble("45"));  // 45
  // 需要四舍五入可以使用Math.round(), 传入double返回long。（传入float返回int）
  long round = Math.round(Double.parseDouble("45.608"));
  System.out.println(round);  // 46
  System.out.println(Math.round(Double.parseDouble("45.600")));  // 46
  System.out.println(Math.round(Double.parseDouble("45.500")));  // 46
  System.out.println(Math.round(Double.parseDouble("45.499")));  // 45
  System.out.println(Math.round(Double.parseDouble("45.450")));  // 45
  System.out.println(Math.round(Double.parseDouble("45.449")));  // 45
  ```



## 二：增强for循环

- JDK1.2出现了Iterator类（`迭代器`）
- 增强for循环中，数据修改不会影响到循环原数据（`示例三`）

```java
Map<String,Object> map = new HashMap<>();
map.put("DFXYCS_001", "45.89");
map.put("DFXYCS_002", "45.01");
map.put("DFXYCS_003", "44.55");
Set<String> keySet = map.keySet();
for(String key : keySet){
    System.out.println(key);
}
```

```java
int[] arr = {1,4,5,8,7,9};
for(int i : arr){
    System.out.println(i);
}
```



## 三：可变参数

```java
public class T03_VariableParam {
    public static void main(String[] args) {
        int[] ints1 = test01(1, 5, 7, 8, 9);
        System.out.println(Arrays.toString(ints1));
        int[] ints2 = test02(1, 5, 7, 8, 9);
        System.out.println(Arrays.toString(ints2));
    }

    private static int[] test01(Integer... nums){
        int length = nums.length;
        for(int s : nums){
            s*=2;  // 迭代器不会修改可变参数数组实际值
            System.out.println(s);
        }
        return Arrays.stream(nums).mapToInt(x->x).toArray();
    }
    private static int[] test02(Integer... nums){
        int length = nums.length;
        for(int i=0;i<length;i++){
            nums[i]*=2;  // 一般循环可以修改可变参数数组实际值
            System.out.println(nums[i]);
        }
        return Arrays.stream(nums).mapToInt(x->x).toArray();
    }
}
```



## 四：Arrays.asList(数组) ：数组可以转换为List了（`JDK8中流处理将会使用更加优雅的方式处理`）

- 待转换的数组不能是基本类型数组

- Arrays.asList(数组)转换成的List是Arrays工具类中的私有静态类，不能进行增加与删除操作

  ```java
  public static <T> List<T> asList(T... a) {
      return new ArrayList<>(a);  // 此处ArrayList不是java.util中的ArrayList
  }
  ```

- 如果想要操作List，可以使用ArrayList对其进行封装

  ```java
  Integer[] nums = {1,4,5,87,7,9};
  List<Integer> integers = new ArrayList<>(Arrays.asList(nums));
  integers.remove(0);
  integers.add(999);
  System.out.println(integers);
  ```

  



## 五：枚举enum

https://kylee.blog.csdn.net/article/details/109008048

```java
public class T05_enum {
    public static void main(String[] args) {
        System.out.println(Season.SPRING==Season.SPRING);
        System.out.println(Season.SPRING.equals(Season.SPRING));
    }
    enum Season{
        SPRING,SUMMER,AUTUMN,WINTER
    }
}
```

```java
public enum Season {
    SPRING("SPRING","春暖花开"),
    SUMMER("SUMMER","夏日炎炎"),
    AUTUMN("AUTUMN","秋高气爽"),
    WINTER("WINTER","白雪皑皑");

    String name;
    String desc;

    Season(String name, String desc){
        this.name = name;
        this.desc = desc;
    }

//    @Override
//    public String toString() {
//        return "Season1{" +
//                "name='" + name + '\'' +
//                ", desc='" + desc + '\'' +
//                '}';
//    }
}
```





## * 附加：JDK5中新增了java.util.concurrent包，提供了许多并发操作API



