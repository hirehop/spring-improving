## JDK7五大特性

---

#### 一：try资源自动释放

- 注意1：括号中声明的变量会被自动转换为final常量，不可修改！
- 注意2：括号中常量需要为AutoClosable接口的实现类！（`需要close的资源类几乎都实现了该接口`）

```java
public class T01_AutoClosableTest {
    private static  final Logger logger = LoggerFactory.getLogger(T01_AutoClosableTest.class);

    public void main(String[] args) {
        try (InputStream inputStream = new BufferedInputStream(new FileInputStream("pom.xml"));
             OutputStream outputStream = new BufferedOutputStream(new FileOutputStream("pom_backup.xml"));) {

            byte[] bys = new byte[1024];
            int len = 0;
            while ((len = inputStream.read(bys)) != -1) {
                outputStream.write(bys, 0, len);
            }

        } catch (IOException e) {
            logger.error(e.getMessage());

        }
    }
}
```



#### 二：catch中异常合并

```java
public static void main(String[] args) {
    try (InputStream inputStream = new BufferedInputStream(new FileInputStream("pom.xml"));
         OutputStream outputStream = new BufferedOutputStream(new FileOutputStream("pom_backup.xml"));) {

        byte[] bys = new byte[1024];
        int len = 0;
        while ((len = inputStream.read(bys)) != -1) {
            outputStream.write(bys, 0, len);
        }

        int iNum = 12/0;

    } catch (IOException|ArithmeticException e) {
        logger.error(e.getMessage());
    }
}
```



#### 三：Diamond

- 正常情况

  ```java
  List<String> list = new ArrayList<>();
  ```

- 匿名内部类菱形表达式不支持（JDK9中匿名内部类支持diamond表达式）

  ```java
  public class T06_Diamond {
      public static void main(String[] args) {
          Handler<Integer> intHandler = new Handler<Integer>(1) {
              @Override
              public void handle() {
                  System.out.println(content);
              }
          };
          intHandler.handle();
          Handler<? extends Number> intHandler1 = new Handler<Number>(2) {
              @Override
              public void handle() {
                  System.out.println(content);
              }
          };
          intHandler1.handle();
          Handler<?> handler = new Handler<Object>("test") {
              @Override
              public void handle() {
                  System.out.println(content);
              }
          };
          handler.handle();
      }
  }
  abstract class Handler<T> {
      public T content;
  
      public Handler(T content) {
          this.content = content;
      }
  
      abstract void handle();
  }
  ```

  

#### 四：0b与10_000

```java
int iNum1 = 0b0110;
int iNum2 = 10_000_000;
System.out.println(iNum1);  // 6
System.out.println(iNum2);  // 10000000
```



#### 五：switch支持String

```java
String season = "spring";
switch(season){
    case "spring":
        System.out.println("spring");
        break;
    case "summer":
        System.out.println("summer");
        break;
    case "autumn":
        System.out.println("autumn");
        break;
    case "winter":
        System.out.println("winter");
        break;
    default:
        System.out.println("NOT SUPPORTED");
        break;
}
```

