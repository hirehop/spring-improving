## JDK8五大特性（自用）

---

#### 一：Lambda表达式



#### 二：方法引用与::

`以自定义排序为例`

```java
public static int compareTo(Student s1, Student s2) {
    if(s1.getName().compareTo(s2.getName())==0){
        return s1.getAge()-s2.getAge();
    }else{
        return s1.getName().compareTo(s2.getName());
    }
}
```

```java
public class T01_BiConsumerTest {
    public static void main(String[] args) {
        BiConsumer<T01_BiConsumerTest,String> test1 = T01_BiConsumerTest::test1;
        test1.accept(new T01_BiConsumerTest(), "xiaoming");
    }

    private void test1(String sName){
        List<Integer> list = new ArrayList<>();
        list.add(15);
        list.add(88);
        list.add(25);
        list.sort(Integer::compareTo);
        list.sort((a,b)->-1);
        System.out.println(list);


        List<Student> studentList = new ArrayList<>();
        Student s1 = new Student("xiaoming723",66);
        Student s2 = new Student("xiaoming123",12);
        Student s3 = new Student("xiaoming234",14);
        Student s4 = new Student("xiaoming234",11);
        studentList.add(s1);
        studentList.add(s2);
        studentList.add(s3);
        studentList.add(s4);
        studentList.sort(Student::compareTo);
        System.out.println(studentList);
    }
}
```



#### 三：接口中默认方法

- default
- static



#### 四：Stream面向集合的流式编程

1. int数组与List之间的转换

   ```xml
   <dependency>
       <groupId>com.google.guava</groupId>
       <artifactId>guava</artifactId>
       <version>30.1-jre</version>
   </dependency>
   ```

   

   ```java
   /* private <A> A[] toArray(IntFunction<A[]> generator); */
   int[] ints = Lists.newArrayList(16, 45, 56).stream().mapToInt(x -> x).toArray();
   //Integer[] ints1 = Lists.newArrayList(16, 45, 56).toArray(new Integer[0]);
   Integer[] ints1 = Lists.newArrayList(16, 45, 56).stream().toArray(Integer[]::new);
   String[] strings = Lists.newArrayList("hello", "world").stream().toArray(String[]::new);
   
   List<Integer> collect = Arrays.stream(ints).boxed().collect(Collectors.toList());
   List<Integer> collect1 = Arrays.stream(ints1).collect(Collectors.toList());
   List<String> collect2 = Arrays.stream(strings).collect(Collectors.toList());
   ```

2.  ArrayList 和 LinkedList 都提供了 toArray(T[] o) 方法

   ```java
   LinkedList<Integer> integers2 = Lists.<Integer>newLinkedList();
   integers2.add(14);
   integers2.add(15);
   Integer[] integers3 = integers2.toArray(new Integer[0]);
   Integer[] integers4 = integers2.toArray(new Integer[6]);
   ```

   ```java
   /* ArrayList中的toArray */
   public <T> T[] toArray(T[] a) {
       if (a.length < size)
           // Make a new array of a's runtime type, but my contents:
           return (T[]) Arrays.copyOf(elementData, size, a.getClass());
       System.arraycopy(elementData, 0, a, 0, size);
       if (a.length > size)
           a[size] = null;
       return a;
   }
   ```

   ```java
   /* 模拟toArray实现 */
   public static <T> T[] toArray(List<T> list, T[] a) {
       //System.out.println(a.getClass().getComponentType());  // class java.lang.Integer
       //System.out.println(Integer.class.getComponentType());  // class java.lang.Integer
       //System.out.println(new int[10].getClass().getComponentType()); // int
       int size = list.size();
       Object[] elementData = new Object[size];
       for(int i=0;i<size;i++){
           elementData[i] = list.get(i);
       }
       if (a.length < size) {
           return (T[]) Arrays.copyOf(elementData, size, a.getClass());
       }
       System.arraycopy(elementData, 0, a, 0, size);
       if (a.length > size)
           a[size] = null;
       return a;
   }
   ```

3. stream流处理（`归纳`）

   - stream()
   - parallelStream()
   - collect(Collectors.asList())
   - colllect(Collectors.joining(","))
   - summaryStatistics()
     - getMax()
     - getMin()
     - getSum()
     - getCount()
     - getAverage()
   - filter(LAMBDA)
   - sorted(LAMBDA)
   - map(LAMBDA)
   - distinct()
   - count()
   - limit(N)

   ```java
   List<String> list = new ArrayList<>();
   list.add("45");
   list.add("95");
   list.add("45");
   // 非String类型元素不能拼接
   System.out.println(String.join(",", list));
   String[] arr = {"aaa","bbb","ccc"};
   System.out.println(String.join(",", arr));
   
   // 统计
   List<Integer> numList = new ArrayList<>();
   numList.add(45);
   numList.add(88);
   numList.add(74);
   numList.add(74);
   numList.add(0);
   numList.add(31);
   System.out.println(numList.stream().mapToInt(x -> x).summaryStatistics().getMax());
   System.out.println(numList.stream().mapToInt(x -> x).summaryStatistics().getMin());
   System.out.println(numList.stream().mapToInt(x -> x).summaryStatistics().getSum());
   System.out.println(numList.stream().mapToInt(x -> x).summaryStatistics().getCount());
   System.out.println(numList.stream().mapToInt(x -> x).summaryStatistics().getAverage());
   
   // 由大到小排序去重过滤0，所有值+1后取前三条数据拼接为逗号分隔的字符串
   String res = numList.stream().sorted(Comparator.reverseOrder()).distinct().filter(x -> x != 0)
   .map(x -> String.valueOf(x + 1)).limit(3).collect(Collectors.joining(","));
   System.out.println(res);
   ```
- `排序Map`
  
   ```java
   Map<String,Integer> map = new HashMap<>();
   map.put("aLUCY",1);
   map.put("BLUCY",8);
   map.put("cLUCY",2);
   map.put("DLUCY",7);
   
   HashMap<String, Integer> map2 = map.entrySet().stream().sorted(Map.Entry.comparingByKey(Comparator.reverseOrder())).
       collect(Collectors.toMap(
       Map.Entry::getKey,
       Map.Entry::getValue,
       (o, n) -> o,
       LinkedHashMap::new
   ));
   System.out.println(map2);
   
   HashMap<String, Integer> map3 = map.entrySet().stream().sorted(Map.Entry.comparingByValue(Comparator.reverseOrder())).
       collect(Collectors.toMap(
    Map.Entry::getKey,
       Map.Entry::getValue,
       (o, n) -> o,
       LinkedHashMap::new
   ));
   
   System.out.println(map3);
   ```
   
   





#### 五：新增的时间类

`暂时使用JodaTime包`


