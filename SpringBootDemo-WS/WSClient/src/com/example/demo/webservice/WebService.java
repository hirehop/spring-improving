package com.example.demo.webservice;

import com.example.demo.HelloWebServiceImpl;

public class WebService {
    public static void main(String[] args) {
        HelloWebServiceImpl service =  new HelloWebServiceImpl();
        String res = service.getHelloWebServiceImplPort().hello("WebService!");
        System.out.println(res);

        String realData = service.getHelloWebServiceImplPort().getRealData("['tag1','tag2','tag3','tag4']");
        System.out.println(realData);
    }
}
