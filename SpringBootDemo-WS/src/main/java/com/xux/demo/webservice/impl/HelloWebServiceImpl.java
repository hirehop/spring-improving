package com.xux.demo.webservice.impl;

import com.xux.demo.webservice.HelloWebService;
import org.springframework.context.annotation.Configuration;

import javax.jws.WebParam;
import javax.jws.WebService;

/**
 * @author X
 * @date 2021-02-25 16:47
 */
@WebService(
        targetNamespace = "demo.example.com",
        serviceName = "HelloWebService",
        endpointInterface = "com.xux.demo.webservice.HelloWebService"
)
@Configuration
public class HelloWebServiceImpl implements HelloWebService {
    @Override
    public String hello(@WebParam(name="info")String info) {
        return "hello "+info;
    }
}
