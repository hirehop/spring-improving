package com.xux.demo.webservice;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

/**
 * @author X
 * @date 2021-02-25 16:46
 */
@WebService
public interface HelloWebService {
    @WebMethod
    String hello(String info);

    @WebMethod
    String getRealData(String jsonData);
}
