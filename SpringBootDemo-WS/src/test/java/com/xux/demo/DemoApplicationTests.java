package com.xux.demo;

import org.json.JSONArray;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@SpringBootTest
class DemoApplicationTests {

	@Test
	void contextLoads() {
		List<String> list = new ArrayList<>();
		list.add("tag1");
		list.add("tag2");
		list.add("tag3");
		list.add("tag4");
		System.out.println(new JSONArray(list).toString());
	}

}
