package com.xux.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Description
 *
 * @Create by X
 * @Date 2021/2/8 20:12
 */
@Controller
public class PageController {

    @RequestMapping("/page1")
    public String index(){
        return "page/page1";
    }

}
