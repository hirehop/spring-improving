package com.xux.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Description
 *
 * @Create by X
 * @Date 2021/2/8 19:46
 */
@RestController
public class HelloController {

    @RequestMapping("/hello")
    public String helloWorld(){
        return "Hello,Xux";
    }
}
