# 注意事项

---

### 一：全注解配置



### 二：Lombok需要IDEA插件支持，同时需要引入依赖

- https://plugins.jetbrains.com/idea

```xml
<dependency>
    <groupId>org.projectlombok</groupId>
    <artifactId>lombok</artifactId>
    <version>1.18.18</version>
    <scope>provided</scope>
</dependency>
```



### 三：对于数据库中blob类型数据在java中使用byte[]映射

