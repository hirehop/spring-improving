package com.x.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author xux
 * @date 2021-02-08 13:42
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class BigData {
    @TableId
    private String id;
    @TableField("dataCondition")
    private String dataCondition;
    @TableField("jsonData")
    private byte[] jsonData;

}

