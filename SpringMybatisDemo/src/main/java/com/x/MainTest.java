package com.x;

import com.x.config.AppConfig;
import com.x.entity.BigData;
import com.x.mapper.BigDataMapper;
import com.xux.falicity.framework.util.FStringUtils;
import com.xux.falicity.framework.util.ZLibUtils;
import org.activiti.engine.impl.util.json.JSONObject;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import java.nio.charset.StandardCharsets;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

/**
 * @Desc
 * @Date 2021-01-05 22:13
 * @Author xux
 */
public class MainTest {
    public static void main(String[] args) throws SQLException {
        ApplicationContext applicationContext = new AnnotationConfigApplicationContext(AppConfig.class);

        // 主配置类注入后，IOC容器中注入了多少类
        for (String s : applicationContext.getBeanDefinitionNames()) {
            System.out.println(s);
        }


        // UserMapper userMapper = applicationContext.getBean(UserMapper.class);
        // userMapper.insert(new User("179074001", "xiaoming", 18, "男"));
        // userMapper.insert(new User("179074002", "xiaopeng", 18, "男"));
        // List<User> all = userMapper.getAll();
        // User user = userMapper.getUserByColumn("id", "179074001");
        // System.out.println(user);


        BigDataMapper bigDataMapper = applicationContext.getBean(BigDataMapper.class);

        // 新增一条数据
        String id = FStringUtils.getUUID();
        DateTimeFormatter formatYMDHMS = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss");
        String strDateTime = new DateTime().toString(formatYMDHMS);
        Map<String, Object> map = new HashMap<>();
        map.put("infoA", "信息A");
        map.put("infoB", "信息B");

        String jsonData = new JSONObject(map).toString();
        BigData data1 = new BigData();
        data1.setId(id);
        data1.setDataCondition(strDateTime);
        data1.setJsonData(ZLibUtils.compress(jsonData.getBytes(StandardCharsets.UTF_8)));
        bigDataMapper.insert(data1);
//
        // 取出一条数据（id）
        BigData bigData = bigDataMapper.get(id);
        String jsonRes = new String(ZLibUtils.decompress(bigData.getJsonData()), StandardCharsets.UTF_8);
        JSONObject jsonObject = new JSONObject(jsonRes);
        System.out.println(jsonObject);
    }
}
