package com.x.mapper;

import com.x.entity.BigData;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

/**
 * @author xux
 * @date 2021-02-08 13:44
 */
public interface BigDataMapper {

    @Insert("INSERT INTO BIGDATA VALUES(#{id},#{dataCondition},#{jsonData})")
    void insert(BigData bigData);

    @Delete("DELETE FROM BIGDATA WHERE ID=#{id}")
    void delete(String id);

    @Update("UPDATE BIGDATA SET ID=#{id},DATACONDITION=#{dataCondition},JSONDATA=#{jsonData}")
    BigData update(BigData bigData);

    @Select("SELECT * FROM BIGDATA WHERE ID=#{id}")
    BigData get(String id);


}
