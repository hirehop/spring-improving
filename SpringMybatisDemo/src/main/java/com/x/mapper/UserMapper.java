package com.x.mapper;

import com.x.entity.User;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * @Desc
 * @Date 2021-01-05 22:01
 * @Author xux
 */
public interface UserMapper {

    @Select("SELECT * FROM USER")
    List<User> getAll();

    @Select("SELECT * FROM USER WHERE ${column}=#{value}")
    User getUserByColumn(@Param("column") String column, @Param("value") String value);

    @Insert("INSERT INTO USER VALUES(#{id},#{name},#{age},#{gender})")
    void insert(User user);
}
