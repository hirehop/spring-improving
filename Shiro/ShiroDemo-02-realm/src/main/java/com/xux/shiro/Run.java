package com.xux.shiro;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.config.IniSecurityManagerFactory;
import org.apache.shiro.mgt.SecurityManager;
import org.apache.shiro.subject.Subject;
import org.apache.shiro.util.Factory;
import org.junit.Assert;
import org.junit.Test;

/**
 * 收集用户身份 / 凭证，即如用户名 / 密码；
 * <p>
 * 调用 Subject.login 进行登录，如果失败将得到相应的 AuthenticationException 异常，根据异常提示用户错误信息；否则登录成功；
 * <p>
 * 最后调用 Subject.logout 进行退出操作。
 *
 * @author xux
 * @date 2021-02-08 17:22
 */
public class Run {

    @Test
    public void testHelloWorld() {
        Factory<SecurityManager> iniSecurityManagerFactory = new IniSecurityManagerFactory("classpath:shiro-multi-realm.ini");
        SecurityManager securityManager = iniSecurityManagerFactory.getInstance();
        SecurityUtils.setSecurityManager(securityManager);

        Subject subject = SecurityUtils.getSubject();
        UsernamePasswordToken token = new UsernamePasswordToken("userA", "123");

        try {
            subject.login(token);
        } catch (AuthenticationException e) {
            System.out.println("身份验证失败。");

        }

        Assert.assertTrue("当前用户未授权。", subject.isAuthenticated());

        System.out.println("登出");
        subject.logout();
    }

}
