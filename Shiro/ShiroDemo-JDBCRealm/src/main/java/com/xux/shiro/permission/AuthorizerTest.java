package com.xux.shiro.permission;

import org.junit.Assert;
import org.junit.Test;

/**
 * @author xux
 * @date 2021-02-18 10:37
 */
public class AuthorizerTest extends BaseTest {
    @Test
    public void testIsPermitted() {
        login("classpath:shiro-authorizer.ini", "zhang", "123");
        //判断拥有权限：user:create
        Assert.assertTrue(subject().isPermitted("user1:view"));
        Assert.assertTrue(subject().isPermitted("user2:update"));
        // 通过二进制位的方式表示权限
        // 权限解析会调用Permission接口中的implies方法
        // BitPermission中重写了该接口中的该方法
        System.out.println(subject().isPermitted("+user1+0"));  // 0000
        System.out.println(subject().isPermitted("+user1+1"));  // 0001
        System.out.println(subject().isPermitted("+user1+2"));  // 0010
        System.out.println(subject().isPermitted("+user1+8"));  // 1000
        System.out.println(subject().isPermitted("+user2+10")); // 1010
        System.out.println(subject().isPermitted("+user1+4"));  // 0100
        System.out.println(subject().isPermitted("menu:view")); // 通过MyRolePermissionResolver解析得到的权限
    }
}
