package com.xux.shiro;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.UnknownAccountException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.config.IniSecurityManagerFactory;
import org.apache.shiro.mgt.SecurityManager;
import org.apache.shiro.subject.PrincipalCollection;
import org.apache.shiro.subject.Subject;
import org.apache.shiro.util.Factory;
import org.junit.Assert;
import org.junit.Test;

/**
 * @author xux
 * @date 2021-02-09 15:14
 */
public class AuthenticatorTest {
    private void login(String configFile, String username, String password){
        Factory<SecurityManager> factory = new IniSecurityManagerFactory(configFile);
        SecurityManager securityManager = factory.getInstance();
        SecurityUtils.setSecurityManager(securityManager);

        Subject subject = SecurityUtils.getSubject();
        UsernamePasswordToken token = new UsernamePasswordToken(username, password);

        subject.login(token);
    }

    @Test
    public void testAllSuccessfulStrategyWithSuccess() {
        login("classpath:shiro-authenticator-all-success.ini", "userA", "123");
        PrincipalCollection principalCollection = subject().getPrincipals();
        // Assert.assertEquals(2, principalCollection.asList().size());
        System.out.println(principalCollection.asList());
    }

    //@Test(expected = UnknownAccountException.class)
    @Test
    public void testAllSuccessfulStrategyWithFail() {
        try {
            login("classpath:shiro-authenticator-all-fail.ini", "userA", "123");
            PrincipalCollection principalCollection = subject().getPrincipals();
            // Assert.assertEquals(2, principalCollection.asList().size());
            System.out.println(principalCollection.asList());
        }catch(UnknownAccountException e){
            System.out.println("身份验证失败！");
        }
    }

    public Subject subject() {
        return SecurityUtils.getSubject();
    }
}
