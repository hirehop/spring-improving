package com.xux.shiro.permission;

import org.apache.shiro.authz.Permission;
import org.apache.shiro.authz.permission.RolePermissionResolver;
import org.apache.shiro.authz.permission.WildcardPermission;

import java.util.Collection;
import java.util.Collections;

/**
 * @author xux
 * @date 2021-02-18 10:03
 */
public class MyRolePermissionResolver implements RolePermissionResolver {
    @Override
    public Collection<Permission> resolvePermissionsInRole(String roleString) {
        // 如果用户拥有 role1，那么就返回一个 “menu:*” 的权限
        if("role1".equals(roleString)) {
            return Collections.singletonList((Permission) new WildcardPermission("menu:*"));
        }
        return null;
    }
}
