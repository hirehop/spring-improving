package com.xux.shiro.test;

import org.apache.shiro.crypto.hash.Md5Hash;
import org.apache.shiro.crypto.hash.Sha256Hash;
import org.apache.shiro.crypto.hash.SimpleHash;

/**
 * @author X
 * @date 2021-02-19 16:32
 */
public class T01_Encode1 {
    public static void main(String[] args) {
        String str = "hello world, 时间！";
        String salt = "0110";
        //还可以转换为 toBase64()/toHex()
        String md5 = new Md5Hash(str, salt).toString();
        System.out.println(md5+" & "+md5.length());

        String sha1 = new Sha256Hash(str, salt).toString();
        System.out.println(sha1+" & "+sha1.length());

        /* 通过调用 SimpleHash 时指定散列算法，其内部使用了 Java 的 MessageDigest 实现 */

        String simpleHash = new SimpleHash("SHA-1", str, salt).toString();
        System.out.println(simpleHash+" & "+simpleHash.length()); // 40

        String simpleHash4 = new SimpleHash("MD5", str, "999").toString();
        System.out.println(simpleHash4+" & "+simpleHash4.length());  // 32

        String simpleHash5 = new SimpleHash("SHA-256", str, "999").toString();
        System.out.println(simpleHash5+" & "+simpleHash5.length());  // 64

        String simpleHash3 = new SimpleHash("SHA-512", str, "999").toString();
        System.out.println(simpleHash3+" & "+simpleHash3.length());  // 128

    }
}
