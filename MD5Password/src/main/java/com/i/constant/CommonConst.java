package com.i.constant;

/**
 *
 * [ 通用常量 ]
 *
 * @version 1.0
 * @since JDK1.8
 */
public class CommonConst {

    public static final class Password {
        public static final String DEFAULT = "123456";
        public static final String SALT_FORMAT = "xiang_%s_admin";
    }

}