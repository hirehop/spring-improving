package com.i.main;

import com.i.constant.CommonConst;
import com.i.util.SmartDigestUtil;

public class Run {
    public static void main(String[] args) {

        String loginPwd = SmartDigestUtil.encryptPassword(CommonConst.Password.SALT_FORMAT, "1234567");

        // 68ecda6c2ece2fa3c7df2ed2c45f18af
        System.out.println(loginPwd);
    }
}
