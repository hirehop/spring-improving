package com.xux.falicity.observerPT;

/**
 * Description
 *
 * @Create by X
 * @Date 2021/1/28 20:56
 */
public class Observer2 extends Observer{

    public Observer2(Subject subject){
        this.subject = subject;
        this.subject.on(this);
    }

    @Override
    public void update() {
        System.out.println("Observer2观察到了新的值："+this.subject.getState());
    }
}
