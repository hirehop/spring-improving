package com.xux.falicity.observerPT;

/**
 * Description
 *
 * @Create by X
 * @Date 2021/1/28 20:49
 */
public class Test {
    public static void main(String[] args) {
        Subject subject = new Subject();
        Observer observer1= new Observer1(subject);
        Observer observer2= new Observer2(subject);

        subject.setState(156);
    }
}
