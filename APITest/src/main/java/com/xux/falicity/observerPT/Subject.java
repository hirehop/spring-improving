package com.xux.falicity.observerPT;

import java.util.ArrayList;
import java.util.List;

/**
 * Description
 *
 * @Create by X
 * @Date 2021/1/28 20:37
 */
public class Subject {
    private int state;
    private List<Observer> observers = new ArrayList<>();

    public Subject(){}

    // 绑定观察者
    public void on(Observer observer){
        observers.add(observer);
    }

    // 状态改编触发事件
    public void setState(int newState){
        this.state = newState;
        emit();
    }

    // 对外提供观察者可以获取的接口
    public int getState(){
        return this.state;
    }

    // 通知所有观察者
    // public权限使得Subject有能力手动推送当前内容
    public void emit(){
        for(Observer o : observers){
            o.update();
        }
    }
}
