package com.xux.falicity.observerPT;

/**
 * Description
 *
 * @Create by X
 * @Date 2021/1/28 20:39
 */
public abstract class Observer {
    public Subject subject;

    public abstract void update();

}
