package com.xux.falicity.observerPT;

/**
 * Description
 *
 * @Create by X
 * @Date 2021/1/28 20:41
 */
public class Observer1 extends Observer{

    public Observer1(Subject subject){
        this.subject = subject;
        this.subject.on(this);
    }

    @Override
    public void update() {
        System.out.println("Observer1观察到了新的值："+this.subject.getState());
    }
}
