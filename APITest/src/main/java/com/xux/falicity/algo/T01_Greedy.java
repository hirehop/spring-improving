package com.xux.falicity.algo;

import java.util.Arrays;

/**
 * 饥饿度与饼干
 *
 * @Create by X
 * @Date 2021/1/26 19:17
 */
public class T01_Greedy {

    public static void main(String[] args) {
        int[] arr1 = {3,4,5};
        int[] arr2 = {2,3,4};
        System.out.println(dealContent(arr1,arr2));
    }

    public static int dealContent(int[] arr1, int[] arr2){
        Arrays.sort(arr1);
        Arrays.sort(arr2);
        int content;
        int child = content = 0;
        while(child<arr1.length && content<arr2.length){
            if(arr1[child]<=arr2[content]){
                ++child;
            }
            ++content;
        }
        return child;
    }
}
