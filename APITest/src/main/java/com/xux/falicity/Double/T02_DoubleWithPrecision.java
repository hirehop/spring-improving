package com.xux.falicity.Double;

/**
 * Double类型数据转换成字符串时，大数字自动转换为科学计数法解决方案
 * 1.NumberFormat
 * 2.String.format设置精度
 *
 * @author xux
 * @date 2021-02-01 10:53
 */
public class T02_DoubleWithPrecision {
    public static void main(String[] args) {
        Double d = 456789123458.23;
        System.out.println(d);  // 4.5678912345823E11

        int precisionTag = 4;
        String precision = "%."+precisionTag+"f";
        System.out.println(String.format(precision,d));  // 456789123458.2300
    }
}
