package com.xux.falicity.Double;

import java.text.NumberFormat;

/**
 * Description
 *
 * @Create by X
 * @Date 2021/1/31 12:30
 */
public class T01_Double {
    public static void main(String[] args) {
        double d = 12345678910.123;
        Double aDouble = d;

        // 直接输出基本类型，会调用其封装类的toString()
        System.out.println(d);
        System.out.println(aDouble);

        // 取消科学计数法显示
        NumberFormat numberFormat = NumberFormat.getInstance();
        numberFormat.setGroupingUsed(false);
        String dd = numberFormat.format(d);
        System.out.println(dd);
    }
}
