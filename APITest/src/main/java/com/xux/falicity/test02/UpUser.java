package com.xux.falicity.test02;

/**
 * @author xux
 * @date 2021-02-05 18:26
 */
public class UpUser {
    private String name;
    private User user;

    public UpUser() {
    }

    public UpUser(String name, User user) {
        this.name = name;
        this.user = user;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @Override
    public String toString() {
        return "UpUser{" +
                "name='" + name + '\'' +
                ", user=" + user +
                '}';
    }
}
