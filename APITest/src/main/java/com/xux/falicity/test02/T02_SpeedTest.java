package com.xux.falicity.test02;

import org.activiti.engine.impl.util.json.JSONObject;

/**
 * @author xux
 * @date 2021-02-05 18:29
 */
public class T02_SpeedTest {
    public static void main(String[] args) {

        JSONObject jsonObject = new JSONObject();
        for (int i = 0; i < 1_000_000; i++) {
            jsonObject.put(String.valueOf(i), "test");
        }
        long start = System.currentTimeMillis();
        for (int i = 0; i < 1_000_000; i++) {
            jsonObject.get(String.valueOf(i));
        }
        long end = System.currentTimeMillis();
        System.out.println(end - start);
    }
}
