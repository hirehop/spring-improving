package com.xux.falicity.test02;

import com.google.gson.Gson;
import org.activiti.engine.impl.util.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * @author xux
 * @date 2021-02-05 17:14
 */
public class T01_JSONObject {
    public static void main(String[] args) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("infoA", "信息A");
        jsonObject.put("infoB", "信息B");
        jsonObject.put("infoC", "信息C");
        System.out.println(jsonObject);

        Map<String, Object> map = new HashMap<>();
        map.put("infoA", "信息A");
        map.put("infoB", "信息B");
        map.put("infoC", "信息C");

        User user = new User("Jack");
        UpUser upUser = new UpUser("upJack", user);

        JSONObject jsonObject2 = new JSONObject();
        jsonObject2.put("infoA", "信息A");
        Map<String, Object> map2 = new HashMap<>();
        map2.put("infoAA", "信息AA");
        map2.put("map1", upUser);
        map2.put("map2", user);
        map2.put("map3", map);
        map2.put("map4", map);
        jsonObject2.put("map", map2);
        Map map3 = new Gson().fromJson(jsonObject2.toString(), Map.class);
        System.out.println(map3);
        System.out.println(map3.get("map"));
        System.out.println("===========");

        System.out.println(jsonObject2);

    }
}
