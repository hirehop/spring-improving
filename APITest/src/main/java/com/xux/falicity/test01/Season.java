package com.xux.falicity.test01;

/**
 * @Date 2021-01-19 22:38
 * @Author XuX
 */

/**
 * enum默认会重写toString
 * <p>
 * 如果只有一个属性：
 * SPRING,SUMMER,AUTUMN,WINTER; 即可
 */
public enum Season {
    SPRING("SPRING", "春暖花开"),
    SUMMER("SUMMER", "夏日炎炎"),
    AUTUMN("AUTUMN", "秋高气爽"),
    WINTER("WINTER", "白雪皑皑");

    String name;
    String desc;

    Season(String name, String desc) {
        this.name = name;
        this.desc = desc;
    }

//    @Override
//    public String toString() {
//        return "Season1{" +
//                "name='" + name + '\'' +
//                ", desc='" + desc + '\'' +
//                '}';
//    }
}
