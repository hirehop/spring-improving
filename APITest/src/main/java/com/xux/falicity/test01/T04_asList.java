package com.xux.falicity.test01;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @Date 2021-01-19 22:24
 * @Author XuX
 */
public class T04_asList {
    public static void main(String[] args) {

        Integer[] nums = {1, 4, 5, 87, 7, 9};
        List<Integer> integers = new ArrayList<>(Arrays.asList(nums));
        integers.remove(0);
        integers.add(999);
        System.out.println(integers);
    }
}
