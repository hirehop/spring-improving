package com.xux.falicity.test01;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Alt+Shift+M 提取方法
 *
 * @Date 2021-01-19 19:57
 * @Author XuX
 */
public class T01_GenericType {
    private static Logger logger = LoggerFactory.getLogger(T01_GenericType.class);

    public static void main(String[] args) {
        // test1();
        // test2();
    }

    private static void test2() {
        // 值可能为Integer、Double(全部作为double处理)
        Map<String, Double> map = new HashMap<>();
        map.put("key1", 12.589);
        map.put("key2", 82.589);
        Double v = map.get("key1") + map.get("key2");
        System.out.println(v);
    }


    private static void test1() {
        List<Integer> list = new ArrayList<>();
        list.add(15);
        list.add(30);
        int res = list.get(0) + list.get(1);
        System.out.println(res);
        List<Double> list1 = new ArrayList<>();
        list1.add(15.0);
        list1.add(30.0);
        Double res1 = list1.get(0) + list1.get(1);
        System.out.println(res1);
    }
}
