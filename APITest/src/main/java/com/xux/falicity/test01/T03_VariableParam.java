package com.xux.falicity.test01;

import java.util.Arrays;

/**
 * @Date 2021-01-19 22:11
 * @Author XuX
 */
public class T03_VariableParam {
    public static void main(String[] args) {
        int[] ints1 = test01(1, 5, 7, 8, 9);
        System.out.println(Arrays.toString(ints1));
        int[] ints2 = test02(1, 5, 7, 8, 9);
        System.out.println(Arrays.toString(ints2));
    }

    private static int[] test01(Integer... nums) {
        int length = nums.length;
        for (int s : nums) {
            s *= 2;  // 迭代器不会修改可变参数数组实际值
            System.out.println(s);
        }
        return Arrays.stream(nums).mapToInt(x -> x).toArray();
    }

    private static int[] test02(Integer... nums) {
        int length = nums.length;
        for (int i = 0; i < length; i++) {
            nums[i] *= 2;  // 一般循环可以修改可变参数数组实际值
            System.out.println(nums[i]);
        }
        return Arrays.stream(nums).mapToInt(x -> x).toArray();
    }
}
