package com.xux.falicity.test01;

import java.net.MalformedURLException;
import java.net.URL;

/**
 * Description
 *
 * @Create by X
 * @Date 2021/1/25 19:11
 */
public class T09_URL {
    public static void main(String[] args) throws MalformedURLException {
        URL url = new URL("http://c.biancheng.net:80/view/1205.html");
        System.out.println(url.getProtocol());  // http
        System.out.println(url.getHost());  // c.biancheng.net
        System.out.println(url.getPath());  // /view/1205.html
        System.out.println(url.getPort());  // 80
    }
}
