package com.xux.falicity.test01;

/**
 * @Date 2021-01-19 22:34
 * @Author XuX
 */
public class T05_enum {
    public static void main(String[] args) {
        System.out.println(Season.SPRING == Season.SPRING);
        System.out.println(Season.SPRING.equals(Season.SPRING));
    }

    enum Season {
        SPRING, SUMMER, AUTUMN, WINTER
    }
}


