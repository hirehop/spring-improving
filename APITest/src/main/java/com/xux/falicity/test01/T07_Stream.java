package com.xux.falicity.test01;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @Date 2021-01-20 18:49
 * @Author XuX
 */
public class T07_Stream {

    private static Logger logger = LoggerFactory.getLogger(T07_Stream.class);

    public static void main(String[] args) {
        List<String> list = new ArrayList<>();
        list.add("45");
        list.add("95");
        list.add("45");
        // 非String类型元素不能拼接
        System.out.println(String.join(",", list));
        String[] arr = {"aaa", "bbb", "ccc"};
        System.out.println(String.join(",", arr));

        // 统计
        List<Integer> numList = new ArrayList<>();
        numList.add(45);
        numList.add(88);
        numList.add(74);
        numList.add(74);
        numList.add(0);
        numList.add(31);
        System.out.println(numList.stream().mapToInt(x -> x).summaryStatistics().getMax());
        System.out.println(numList.stream().mapToInt(x -> x).summaryStatistics().getMin());
        System.out.println(numList.stream().mapToInt(x -> x).summaryStatistics().getSum());
        System.out.println(numList.stream().mapToInt(x -> x).summaryStatistics().getCount());
        System.out.println(numList.stream().mapToInt(x -> x).summaryStatistics().getAverage());

        // 由大到小排序去重过滤0，所有值+1后取前三条数据拼接为逗号分隔的字符串
        String res = numList.stream().sorted(Comparator.reverseOrder()).distinct().filter(x -> x != 0)
                .map(x -> String.valueOf(x + 1)).limit(3).collect(Collectors.joining(","));
        System.out.println(res);

        logger.debug("hello");

        try {
            int a = 12 / 0;
        } catch (Exception e) {
            logger.error(e.getMessage());
            e.printStackTrace();
            return;
        }

        System.out.println("hello");
    }
}

