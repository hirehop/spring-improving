package com.xux.falicity.test01;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Description
 *
 * @Create by X
 * @Date 2021/1/25 19:33
 */
public class T10_java8getAverageFromList {
    public static void main(String[] args) {
        List<Integer> list = new ArrayList<>();
        list.add(15);
        list.add(25);
        list.add(75);
        list.add(61);

        AtomicInteger sum = new AtomicInteger();
        list.forEach(sum::addAndGet);
        double average = sum.get() / (double) list.size();

        double average1 = list.stream().mapToInt(x -> x).summaryStatistics().getAverage();

        System.out.println(average);
        System.out.println(average1);
    }
}
