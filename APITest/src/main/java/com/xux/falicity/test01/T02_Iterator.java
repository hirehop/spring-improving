package com.xux.falicity.test01;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * @Date 2021-01-19 22:06
 * @Author XuX
 */
public class T02_Iterator {
    public static void main(String[] args) {
        Map<String, Object> map = new HashMap<>();
        map.put("DFXYCS_001", "45.89");
        map.put("DFXYCS_002", "45.01");
        map.put("DFXYCS_003", "44.55");
        Set<String> keySet = map.keySet();
        for (String key : keySet) {
            System.out.println(key);
        }
    }
}
