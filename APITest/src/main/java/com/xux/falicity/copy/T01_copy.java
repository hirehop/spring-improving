package com.xux.falicity.copy;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

/**
 * @auhor XuX
 * @date 2021/2/2 13:05
 * @desc
 */
public class T01_copy {
    private static Logger logger = LoggerFactory.getLogger(T01_copy.class);

    public static void main(String[] args) throws Exception {
        String fromPath = "src";
        String toPath = "XX";
        copy(fromPath, toPath);
        System.out.println("[Finished]");
    }

    /**
     * 文件拷贝
     * 文件夹 -> 文件夹
     * 文件   -> 文件夹
     *
     * @param srcPath 文件名/文件夹名
     * @param dstPath 目的文件夹名称
     */
    public static void copy(String srcPath, String dstPath) throws Exception {
        srcPath = srcPath.trim();
        dstPath = dstPath.trim();
        // 当前程序所在位置：user.dir
        // IDEA中为当前项目根目录
        File base = new File(System.getProperty("user.dir"));
        try {
            File src = new File(base, srcPath);
            File dst = new File(base, dstPath);
            dst.mkdirs();
            copyFolder(src, dst);
        } catch (Exception e) {
            logger.error(e.getMessage());
            System.out.println("[error] 文件拷贝失败！请联系管理员。");
        }
    }

    private static void copyFolder(File src, File dst) throws IOException {
        if (src.isDirectory()) {
            // 在目标位置新建一个文件夹
            File newFolder = new File(dst, src.getName());
            newFolder.mkdirs();
            File[] files = src.listFiles();
            assert files != null;
            for (File f : files) {
                copyFolder(f, newFolder);
            }
        } else {
            File newFile = new File(dst, src.getName());
            copyFile(src, newFile);
        }
    }

    private static void copyFile(File srcFile, File dstFile) throws IOException {
        BufferedInputStream bis = new BufferedInputStream(new FileInputStream(srcFile));
        BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(dstFile));
        byte[] bys = new byte[1024];
        int len = 0;
        while ((len = bis.read(bys)) != -1) {
            bos.write(bys, 0, len);
        }
    }

    /**
     * zip文件压缩
     *
     * @param inputFile  待压缩文件夹/文件名
     * @param outputFile 生成的压缩包名字
     */

    public static void ZipCompress(String inputFile, String outputFile) throws Exception {
        //创建zip输出流
        ZipOutputStream out = new ZipOutputStream(new FileOutputStream(outputFile));
        //创建缓冲输出流
        BufferedOutputStream bos = new BufferedOutputStream(out);
        File input = new File(inputFile);
        compress(out, bos, input, null);
        bos.close();
        out.close();
    }

    /**
     * @param name 压缩文件名，可以写为null保持默认
     */
    //递归压缩
    private static void compress(ZipOutputStream out, BufferedOutputStream bos, File input, String name) throws IOException {
        if (name == null) {
            name = input.getName();
        }
        //如果路径为目录（文件夹）
        if (input.isDirectory()) {
            //取出文件夹中的文件（或子文件夹）
            File[] flist = input.listFiles();

            if (flist.length == 0)//如果文件夹为空，则只需在目的地zip文件中写入一个目录进入
            {
                out.putNextEntry(new ZipEntry(name + "/"));
            } else//如果文件夹不为空，则递归调用compress，文件夹中的每一个文件（或文件夹）进行压缩
            {
                for (int i = 0; i < flist.length; i++) {
                    compress(out, bos, flist[i], name + "/" + flist[i].getName());
                }
            }
        } else//如果不是目录（文件夹），即为文件，则先写入目录进入点，之后将文件写入zip文件中
        {
            out.putNextEntry(new ZipEntry(name));
            FileInputStream fos = new FileInputStream(input);
            BufferedInputStream bis = new BufferedInputStream(fos);
            int len;
            //将源文件写入到zip文件中
            byte[] buf = new byte[1024];
            while ((len = bis.read(buf)) != -1) {
                bos.write(buf, 0, len);
            }
            bis.close();
            fos.close();
        }
    }
}
