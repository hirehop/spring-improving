package com.xux;

import com.xux.entity.Student;
import com.xux.mapper.StudentMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

/**
 * Description
 *
 * @Create by X
 * @Date 2021/2/8 23:07
 */
@SpringBootTest
public class SampleTest {

    @Autowired
    private StudentMapper studentMapper;

    @Test
    public void testSelect(){
        List<Student> students = studentMapper.selectList(null);
        System.out.println(students);
    }
}
