package com.xux.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author xux
 * @since 2021-02-09
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Bigdata implements Serializable {

    private static final long serialVersionUID = 1L;

    public static final String ID = "ID";
    public static final String DATACONDITION = "DATACONDITION";
    public static final String JSONDATA = "JSONDATA";

    @TableId("ID")
    private String id;

    @TableField("DATACONDITION")
    private String datacondition;

    @TableField("JSONDATA")
    private byte[] jsondata;


}
