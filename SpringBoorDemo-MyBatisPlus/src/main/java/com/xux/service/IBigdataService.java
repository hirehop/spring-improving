package com.xux.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.xux.entity.Bigdata;
import com.xux.vo.BigdataVo;

import java.util.List;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author jobob
 * @since 2021-02-09
 */
public interface IBigdataService extends IService<Bigdata> {

    // void test();

    // 分页查询所有数据
    BigdataVo listByPage(Integer current, Integer size);

    // 查询当前年份所有数据
    List<Bigdata> listCurrentYearData();

    // 分页查询当前年份所有数据
    BigdataVo listCurrentYearDataByPage(Integer current, Integer size);
}
