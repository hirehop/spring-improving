package com.xux.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.xux.entity.Bigdata;
import com.xux.mapper.BigdataMapper;
import com.xux.service.IBigdataService;
import com.xux.vo.BigdataVo;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author jobob
 * @since 2021-02-09
 */
@Service
public class BigdataServiceImpl extends ServiceImpl<BigdataMapper, Bigdata> implements IBigdataService {

    private static DateTimeFormatter formatSSS = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss.SSS");

    @Autowired
    BigdataMapper bigdataMapper;

    @Override
    public BigdataVo listByPage(Integer current, Integer size) {
        BigdataVo bigdataVo = new BigdataVo();
        Page<Bigdata> bigdataPage = new Page<>(current, size);
        bigdataMapper.selectPage(bigdataPage, null);
        bigdataVo.setCurrent(current);
        // bigdataPage.size()和size均为固定大小，为了方便页面中js计算分页总数，不使用当前分页实际数据个数
        bigdataVo.setSize(size);
        bigdataVo.setTotal(bigdataPage.getTotal());
        bigdataVo.setBigdataList(bigdataPage.getRecords());
        return bigdataVo;
    }

    @Override
    public List<Bigdata> listCurrentYearData() {
        DateTime dt = new DateTime();
        DateTime yearStart = new DateTime(dt.getYear(), 1, 1, 0, 0, 0, 0);

        QueryWrapper<Bigdata> bigdataQueryWrapper = new QueryWrapper<>();
        bigdataQueryWrapper.apply("UNIX_TIMESTAMP(datacondition) >= UNIX_TIMESTAMP('" + yearStart.toString(formatSSS) + "')");

        return bigdataMapper.selectList(bigdataQueryWrapper);
    }

    @Override
    public BigdataVo listCurrentYearDataByPage(Integer current, Integer size) {
        DateTime dt = new DateTime();
        DateTime yearStart = new DateTime(dt.getYear(), 1, 1, 0, 0, 0, 0);
        QueryWrapper<Bigdata> bigdataQueryWrapper = new QueryWrapper<>();
        bigdataQueryWrapper.apply("UNIX_TIMESTAMP(datacondition) >= UNIX_TIMESTAMP('" + yearStart.toString(formatSSS) + "')");

        BigdataVo bigdataVo = new BigdataVo();
        Page<Bigdata> bigdataPage = new Page<>(current, size);
        bigdataMapper.selectPage(bigdataPage, bigdataQueryWrapper);
        bigdataVo.setCurrent(current);
        // bigdataPage.size()和size均为固定大小，为了方便页面中js计算分页总数，不使用当前分页实际数据个数
        bigdataVo.setSize(size);
        bigdataVo.setTotal(bigdataPage.getTotal());
        bigdataVo.setBigdataList(bigdataPage.getRecords());

        return bigdataVo;
    }
}
