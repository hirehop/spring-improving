package com.xux.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xux.entity.Student;
import org.apache.ibatis.annotations.Mapper;

/**
 * Description
 *
 * @Create by X
 * @Date 2021/2/8 23:05
 */
@Mapper
public interface StudentMapper extends BaseMapper<Student> {
}
