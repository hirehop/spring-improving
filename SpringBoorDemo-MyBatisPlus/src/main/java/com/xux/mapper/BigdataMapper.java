package com.xux.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xux.entity.Bigdata;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author jobob
 * @since 2021-02-09
 */
@Mapper
public interface BigdataMapper extends BaseMapper<Bigdata> {

}
