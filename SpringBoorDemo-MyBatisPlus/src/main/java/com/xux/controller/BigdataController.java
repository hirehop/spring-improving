package com.xux.controller;


import com.xux.entity.Bigdata;
import com.xux.service.impl.BigdataServiceImpl;
import com.xux.vo.BigdataVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author jobob
 * @since 2021-02-09
 */
@RestController
@RequestMapping("/bigdata")
public class BigdataController {

    @Autowired
    BigdataServiceImpl bigdataService;

//    @RequestMapping("addData")
//    public boolean addBigData() throws SQLException {
//        Bigdata bigdata = new Bigdata();
//        bigdata.setId(UUID.randomUUID().toString().replaceAll("-", ""));
//        bigdata.setDatacondition(new DateTime().toString(formatSSS));
//        byte[] bys = "I LOVE FOOD!".getBytes(StandardCharsets.UTF_8);
//        bigdata.setJsondata(bys);
//        // System.out.println(new String(bys,StandardCharsets.UTF_8));
//        return bigdataService.save(bigdata);
//    }


    /**
     * 查询年初到现在的所有记录
     */
    @RequestMapping("listCurrentYear")
    public List<Bigdata> listCurrentYear() {
        return bigdataService.listCurrentYearData();
    }

    /**
     * 分页查询年初到现在的所有记录
     */
    @RequestMapping("listCurrentYearByPage")
    public BigdataVo listCurrentYearByPage(Integer current, Integer size) {
        return bigdataService.listCurrentYearDataByPage(current, size);
    }

    /**
     * 查询所有记录
     */
    @RequestMapping("list")
    public List<Bigdata> list() {
        return bigdataService.list();
    }

    /**
     * 查询指定分页数据
     */
    @RequestMapping("listByPage")
    public BigdataVo listByPage(Integer current, Integer size) {
        return bigdataService.listByPage(current, size);
    }
}
