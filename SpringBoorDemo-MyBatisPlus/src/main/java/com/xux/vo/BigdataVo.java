package com.xux.vo;

import com.xux.entity.Bigdata;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.RequiredArgsConstructor;

import java.util.List;

/**
 * @author X
 * @date 2021-02-22 10:51
 */
@Data
@AllArgsConstructor
@RequiredArgsConstructor
public class BigdataVo {
    // 当前分页号
    private Integer current;
    // 当前页面数
    private Integer size;
    // 总数据数
    private Long total;
    // 当前分页中数据
    private List<Bigdata> bigdataList;
}
