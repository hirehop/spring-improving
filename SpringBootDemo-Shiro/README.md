# SpringBoot集成Shiro

- 登录身份验证

---

### 技术栈

1. SpringBoot 2.4.2
2. MyBatis Plus 3.4.1
3. Apache Shiro 1.7.1
4. Thymeleaf 



### 建表（testdb库）

```sql
SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `username` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `password` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `roleid` int NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES (1, 'tom', 'tom#8888', 1);

SET FOREIGN_KEY_CHECKS = 1;
```





---

### 使用分析

1. 依赖

   ```xml
   <parent>
       <groupId>org.springframework.boot</groupId>
       <artifactId>spring-boot-starter-parent</artifactId>
       <version>2.4.2</version>
       <relativePath/>
   </parent>
   
   <properties>
       <java.version>1.8</java.version>
   </properties>
   
   <dependencies>
   
       <dependency>
           <groupId>javax.servlet</groupId>
           <artifactId>javax.servlet-api</artifactId>
           <scope>provided</scope>
       </dependency>
   
       <dependency>
           <groupId>org.springframework.boot</groupId>
           <artifactId>spring-boot-starter</artifactId>
       </dependency>
       <dependency>
           <groupId>org.springframework.boot</groupId>
           <artifactId>spring-boot-starter-web</artifactId>
       </dependency>
       <dependency>
           <groupId>org.springframework.boot</groupId>
           <artifactId>spring-boot-starter-thymeleaf</artifactId>
       </dependency>
   
       <!-- MyBatis Plus -->
       <dependency>
           <groupId>com.baomidou</groupId>
           <artifactId>mybatis-plus-boot-starter</artifactId>
           <version>3.4.1</version>
       </dependency>
       <dependency>
           <groupId>com.baomidou</groupId>
           <artifactId>mybatis-plus-generator</artifactId>
           <version>3.4.1</version>
       </dependency>
       <dependency>
           <groupId>org.freemarker</groupId>
           <artifactId>freemarker</artifactId>
           <version>2.3.30</version>
       </dependency>
   
   
       <!-- JDBC -->
       <dependency>
           <groupId>mysql</groupId>
           <artifactId>mysql-connector-java</artifactId>
           <version>8.0.23</version>
       </dependency>
       <dependency>
           <groupId>com.alibaba</groupId>
           <artifactId>druid</artifactId>
           <version>1.2.4</version>
       </dependency>
   
       <!-- 开启cache并集成ehcache -->
       <dependency>
           <groupId>org.springframework.boot</groupId>
           <artifactId>spring-boot-starter-cache</artifactId>
       </dependency>
       <dependency>
           <groupId>net.sf.ehcache</groupId>
           <artifactId>ehcache</artifactId>
       </dependency>
   
       <!-- 集成shiro并使用ehcache缓存 -->
       <dependency>
           <groupId>org.apache.shiro</groupId>
           <artifactId>shiro-spring</artifactId>
           <version>1.7.1</version>
       </dependency>
       <dependency>
           <groupId>org.apache.shiro</groupId>
           <artifactId>shiro-ehcache</artifactId>
           <version>1.7.1</version>
       </dependency>
   
       <!-- Lombok -->
       <dependency>
           <groupId>org.projectlombok</groupId>
           <artifactId>lombok</artifactId>
           <version>1.18.18</version>
           <scope>provided</scope>
       </dependency>
   
   </dependencies>
   ```

   

2. springboot配置文件

   ```yaml
   # 服务器设置
   server:
     port: 8880
     servlet:
       context-path: /
     tomcat:
       uri-encoding: UTF-8
     # 将请求协议转换为 https
     schemeHttps: false
   
   
   # 管理基础路径
   adminPath: /x
   
   # 配置thymeleaf
   spring:
     thymeleaf:
       suffix: .html
       mode: HTML
       encoding: UTF-8
       servlet:
         content-type: text/html
     mvc:
       static-path-pattern: /**
     # JDBC
     datasource:
       type: com.alibaba.druid.pool.DruidDataSource
       driver-class-name: com.mysql.cj.jdbc.Driver
       username: root
       password: ABCD#8888
       url: jdbc:mysql:///testdb?characterEncoding=UTF-8&useUnicode=true&useSSL=false&tinyInt1isBit=false&allowPublicKeyRetrieval=true&serverTimezone=Asia/Shanghai
   ```

3. MP代码生成器配置

4. shiro使用ehcache缓存配置文件

   ```xml
   <?xml version="1.0" encoding="UTF-8"?>
   <ehcache updateCheck="false" name="cacheManagerConfigFile">
       <defaultCache
               maxElementsInMemory="1000"
               eternal="false"
               timeToIdleSeconds="120"
               timeToLiveSeconds="120"
               overflowToDisk="false"
               diskPersistent="false"
               diskExpiryThreadIntervalSeconds="120"
               memoryStoreEvictionPolicy="LRU"/>
        
       <cache name="shiro-activeSessionCache"
              statistics="true"
              maxElementsInMemory="1000"
              eternal="false"
              timeToIdleSeconds="0"
              timeToLiveSeconds="0"
              overflowToDisk="false"
              memoryStoreEvictionPolicy="LRU"/>
   
       <!-- Less Frequently Used, Least Recently Used, First In First Out -->
        
   </ehcache>
   ```

5. shiro配置类

   1. 注册 EhCacheManager（`EH缓存管理器`）

      ```java
      @Bean
      public EhCacheManager getEhCacheManager() {
          EhCacheManager em = new EhCacheManager();
          em.setCacheManagerConfigFile("classpath:ehcache-shiro.xml");
          return em;
      }
      ```

   2. 注册 Reaslm、DefaultWebSessionManager

   3. 注册 DefaultWebSecurityManager

      ```java
      @Bean(name = "securityManager")
      public DefaultWebSecurityManager getDefaultWebSecurityManager(UserRealm userRealm) {
          DefaultWebSecurityManager dwsm = new DefaultWebSecurityManager();
          // 设置Realm
          dwsm.setRealm(userRealm);
          // 采用EhCache 缓存
          dwsm.setCacheManager(getEhCacheManager());
          // 设置会话管理器
          dwsm.setSessionManager(getDefaultWebSessionManager());
          return dwsm;
      }
      ```

   4. 注册 ShiroFilterFactoryBean

      ```java
      @Bean
      public ShiroFilterFactoryBean shiroFilter(org.apache.shiro.mgt.SecurityManager securityManager) {
          ShiroFilterFactoryBean shiroFilterFactoryBean = new ShiroFilterFactoryBean();
          shiroFilterFactoryBean.setSecurityManager(securityManager);
          // 如果不设置默认会自动寻找Web工程根目录下的"/login.jsp"页面
          shiroFilterFactoryBean.setLoginUrl("/user/login");
          shiroFilterFactoryBean.setSuccessUrl("/");
          // shiroFilterFactoryBean.setUnauthorizedUrl("/403");
      
          // 拦截器
          Map<String, String> filterChainDefinitionMap = new LinkedHashMap<>();
          filterChainDefinitionMap.put("/js/**", "anon");
          filterChainDefinitionMap.put("/css/**", "anon");
          /* 所有页面都需要授权后访问 */
          filterChainDefinitionMap.put("/**", "authc");
      
          Map<String, Filter> filters = new HashMap<>();
          shiroFilterFactoryBean.setFilters(filters);
          shiroFilterFactoryBean.setFilterChainDefinitionMap(filterChainDefinitionMap);
          return shiroFilterFactoryBean;
      }
      ```

      `Realm注册后，/user/login页面发送POST请求，会自动进行登录验证`

      `此时，只需要编写GET方式请求的/user/login页面处理逻辑，并在此根据session是否存在进行登录判断`

      ```java
      /* 登录之前任何页面访问都跳转到登录页面 */
      @RequestMapping("login")
      public String login() {
          Subject subject = SecurityUtils.getSubject();
          Session session = subject.getSession(false);
          if (session == null) {
              return "page/login";
          } else {
              return "page/index";
          }
      }
      ```

      

   5. 参考项目内具体配置类其他配置

   


6. 编写MvcController继承WebMvcConfigurer，快速完成基本请求与基本页面的绑定
	```java
	@Configuration
	public class MvcController implements WebMvcConfigurer {
	    @Override
    public void addViewControllers(ViewControllerRegistry registry) {
           registry.addViewController("/").setViewName("page/index");
           registry.addViewController("/index").setViewName("page/index");
           registry.addViewController("/403").setViewName("error/403");
           registry.addViewController("/404").setViewName("error/404");
       }
   }
   ```
   
7. X

8. X












