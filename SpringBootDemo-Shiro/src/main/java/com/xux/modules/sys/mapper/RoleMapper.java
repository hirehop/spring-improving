package com.xux.modules.sys.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xux.modules.sys.entity.Role;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author X
 * @since 2021-02-20
 */
@Mapper
public interface RoleMapper extends BaseMapper<Role> {

}
