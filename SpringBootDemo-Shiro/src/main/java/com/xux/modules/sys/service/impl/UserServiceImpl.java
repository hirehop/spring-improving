package com.xux.modules.sys.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.xux.modules.sys.entity.User;
import com.xux.modules.sys.mapper.UserMapper;
import com.xux.modules.sys.service.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author X
 * @since 2021-02-20
 */
@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements IUserService {

    @Autowired
    private UserMapper userMapper;

    public User selectUserByUsername(String username) {
//        Map<String, Object> params = new HashMap<>();
//        params.put("username", username);
//        List<User> users = userMapper.selectByMap(params);
//        return users.size()<1?null:users.get(0);

        QueryWrapper<User> wrapper = new QueryWrapper<>();
        wrapper.eq("username", username);
        List<User> users = userMapper.selectList(wrapper);
        return users.size() > 0 ? users.get(0) : null;
    }
}
