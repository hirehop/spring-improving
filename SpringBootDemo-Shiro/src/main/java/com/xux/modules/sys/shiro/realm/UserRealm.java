package com.xux.modules.sys.shiro.realm;

import com.xux.modules.sys.entity.User;
import com.xux.modules.sys.service.IUserService;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.SimpleAuthenticationInfo;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @author X
 * @date 2021-02-20 13:25
 */
public class UserRealm extends AuthorizingRealm {

    @Autowired
    private IUserService userService;

    @Override
    protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principals) {
        return null;
    }

    @Override
    protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken token) throws AuthenticationException {
        String username = (String) token.getPrincipal();
        String password = new String((char[]) token.getCredentials());

        AuthenticationInfo info;
        User user = userService.selectUserByUsername(username);
        // 当前用户存在
        if (user != null) {
            if(user.getPassword().equals(password)) {
                info = new SimpleAuthenticationInfo(username, password, getName());
                return info;
            }
        }
        return null;

    }
}
