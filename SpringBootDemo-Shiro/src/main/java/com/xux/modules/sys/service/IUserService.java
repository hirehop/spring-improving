package com.xux.modules.sys.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.xux.modules.sys.entity.User;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author X
 * @since 2021-02-20
 */
public interface IUserService extends IService<User> {

    User selectUserByUsername(String username);
}
