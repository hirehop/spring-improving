package com.xux.modules.sys.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.xux.modules.sys.entity.Role;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author X
 * @since 2021-02-20
 */
public interface IRoleService extends IService<Role> {

}
