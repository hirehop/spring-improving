package com.xux.modules.sys.controller;


import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author X
 * @since 2021-02-20
 */
@Controller
@RequestMapping("/user")
public class UserController {

    @GetMapping("login")
    public String loginGet() {
        return "page/login";
    }
}
