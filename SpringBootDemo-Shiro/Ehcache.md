# ehcache

---

### 一：配置文件 ehcache.xml

```xml
<?xml version="1.0" encoding="UTF-8"?>
<ehcache updateCheck="false" name="cacheManagerConfigFile">

    <!-- 磁盘缓存位置 -->
    <diskStore path="java.io.tmpdir/ehcache"/>

    <!-- 默认缓存 -->
    <defaultCache
            // 内存中允许存储的最大的元素个数，0代表无限个。
            maxElementsInMemory="10000"
            // 设置缓存中对象是否为永久的，如果是，超时设置将被忽略，对象从不过期。根据存储数据的不同，例如一些静态不变的数据如省市区等可以设置为永不过时。
            eternal="false"
            // 设置对象在失效前的允许闲置时间（单位：秒）。仅当eternal=false对象不是永久有效时使用，可选属性，默认值是0，也就是可闲置时间无穷大。
            timeToIdleSeconds="120"
            // 缓存数据的生存时间（TTL），也就是一个元素从构建到消亡的最大时间间隔值，这只能在元素不是永久驻留时有效，如果该值是0就意味着元素可以停顿无穷长的时间。
            timeToLiveSeconds="120"
            // 内存不足时，是否启用磁盘缓存。
            overflowToDisk="false"
            // 是否在VM重启时存储硬盘的缓存数据。默认值是false。
            diskPersistent="false"
            // 磁盘失效线程运行时间间隔，默认是120秒。
            diskExpiryThreadIntervalSeconds="120"
            // 缓存存储策略
            memoryStoreEvictionPolicy="LRU"/>
     
    <!-- 自定义缓存 -->
    <cache name="shiro-activeSessionCache"
           statistics="true"
           maxElementsInMemory="1000"
           eternal="false"
           timeToIdleSeconds="0"
           timeToLiveSeconds="0"
           overflowToDisk="false"
           memoryStoreEvictionPolicy="LRU"/>

    <!-- Less Frequently Used, Least Recently Used, First In First Out -->
     
</ehcache>
```



### 二：@Cacheable注解

```java
// 将缓存保存到名称为UserCache中，键为"user:"字符串加上userId值，如 'user:1'
@Cacheable(value="UserCache", key="'user:' + #userId")
public User findById(String userId) {
    return (User) new User("1", "mengdee");
}
 
// 将缓存保存进UserCache中，并当参数userId的长度小于12时才保存进缓存，默认使用参数值及类型作为缓存的key
// 保存缓存需要指定key，value， value的数据类型，不指定key默认和参数名一样如："1"
@Cacheable(value="UserCache", condition="#userId.length() < 12")
public boolean isReserved(String userId) {
    System.out.println("UserCache:"+userId);
    return false;
}　　
```

**@Cacheable**：表明所修饰的方法是可以缓存的：当第一次调用这个方法时，它的结果会被缓存下来，在缓存的有效时间内，以后访问这个方法都直接返回缓存结果，不再执行方法中的代码段。

**@Cacheable 支持如下几个参数**：

**value**：缓存位置名称，不能为空，如果使用EHCache，就是ehcache.xml中声明的cache的name, 指明将值缓存到哪个Cache中

**key**：缓存的key，默认为空，既表示使用方法的参数类型及参数值作为key，支持SpEL，如果要引用参数值使用井号加参数名，如：#userId，一般来说，我们的更新操作只需要刷新缓存中某一个值，所以定义缓存的key值的方式就很重要，最好是能够唯一，因为这样可以准确的清除掉特定的缓存，而不会影响到其它缓存值 ，本例子中使用实体加冒号再加ID组合成键的名称，如"user:1"、"order:223123"等

**condition**：触发条件，只有满足条件的情况才会加入缓存，默认为空，既表示全部都加入缓存，支持SpEL



