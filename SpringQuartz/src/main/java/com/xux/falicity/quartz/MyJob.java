package com.xux.falicity.quartz;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import java.util.Date;

/**
 * @Date 2021-01-09 22:08
 * @Author XuX
 */
public class MyJob implements Job {
    // 任务被触发时执行的方法
    public void execute(JobExecutionContext jobExecutionContext) throws JobExecutionException {
        System.out.println(new Date());
    }
}
