package com.xux.falicity.quartz;

import org.quartz.*;
import org.quartz.impl.StdSchedulerFactory;

/**
 * @Date 2021-01-08 12:26
 * @Author XuX
 */
public class MainTest {
    public static void main(String[] args) throws SchedulerException {
        // 1.创建Job对象
        JobDetail job = JobBuilder.newJob(MyJob.class).build();

        // 2.创建Trigger对象
        //    2.1 简单的触发时间
        //    2.2 通过corn表达式进行配置触发时间
        Trigger trigger1 = TriggerBuilder.newTrigger().withSchedule(SimpleScheduleBuilder.repeatSecondlyForever())
                .build();
        Trigger trigger2 = TriggerBuilder.newTrigger().withSchedule(CronScheduleBuilder.cronSchedule("0/2 * * * * ?"))
                .build();

        // 3.创建Scheduler对象
        Scheduler scheduler = StdSchedulerFactory.getDefaultScheduler();
        scheduler.scheduleJob(job, trigger2);

        // 4.启动
        scheduler.start();
    }
}
