package com.xux.falicity;

import com.xux.falicity.config.AppConfig;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

/**
 * @Date 2021-01-08 12:29
 * @Author XuX
 */
public class AppMain {
    public static void main(String[] args) {
        ApplicationContext ac = new AnnotationConfigApplicationContext(AppConfig.class);
    }
}
