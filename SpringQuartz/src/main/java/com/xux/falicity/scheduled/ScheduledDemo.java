package com.xux.falicity.scheduled;

import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.Date;

/**
 * @Date 2021-01-08 12:28
 * @Author XuX
 */
@Component
public class ScheduledDemo {
    /**
     * Scheduled定时任务
     */
    @Scheduled(cron = "0/2 * * * * ?")
    public void scheduledMethod() {
        System.out.println(new Date());
    }
}
