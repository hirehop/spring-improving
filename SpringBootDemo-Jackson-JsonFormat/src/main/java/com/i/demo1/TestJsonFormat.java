package com.i.demo1;

import com.alibaba.fastjson.JSON;
import com.i.module.test.domain.StudentEntity;

import java.sql.Date;

public class TestJsonFormat {
    public static void main(String[] args) {
        Date date = new Date(1324564678);
        StudentEntity student = new StudentEntity();
        student.setDate(date);
        System.out.println(JSON.toJSONString(student));
    }
}
