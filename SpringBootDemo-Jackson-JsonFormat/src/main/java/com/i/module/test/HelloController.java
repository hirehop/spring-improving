package com.i.module.test;

import com.i.module.test.domain.StudentEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.sql.Date;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@RestController
public class HelloController {

    private static final DateTimeFormatter formatYMDHMS = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
    private static final DateTimeFormatter formatCHYMDHMS = DateTimeFormatter.ofPattern("yyyy年MM月dd日 HH时mm分ss秒");

    /**
     * [ 后台格式化实体数据，向前端返回json格式的时间 ]
     * @return StudentEntity
     */
    @GetMapping("/test01")
    public StudentEntity test01(){
        StudentEntity studentEntity = new StudentEntity();
        studentEntity.setDate(new Date(1617353272000L));
        return studentEntity;
    }

    /**
     * 前端传入时间，后台解析（http://localhost:8088/test02?dateTime=2020-01-12 2012:58:20）
     * @param dateTime
     * @return
     */
    @GetMapping("/test02")
    public String test02(@RequestParam("dateTime") String dateTime){
        LocalDateTime parse = LocalDateTime.parse(dateTime, formatYMDHMS);
        // 2020-01-12T12:58:20
        System.out.println(parse);
        return parse.format(formatCHYMDHMS);
    }
}
