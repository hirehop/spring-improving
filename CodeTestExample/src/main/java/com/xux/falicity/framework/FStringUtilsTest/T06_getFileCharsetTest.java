package com.xux.falicity.framework.FStringUtilsTest;

import com.xux.falicity.framework.util.FStringUtils;

import java.io.File;
import java.io.IOException;

/**
 * @Date 2021-01-07 22:42
 * @Author XuX
 */
public class T06_getFileCharsetTest {
    public static void main(String[] args) throws IOException {
        // 默认创建路径为user.dir
        File file = new File("hello.temp");
        file.createNewFile();
        String filecharset = FStringUtils.getFileCharset(file);
        System.out.println(filecharset);
    }
}
