package com.xux.falicity.framework.FStringUtilsTest;

import com.xux.falicity.framework.util.FStringUtils;

/**
 * @Date 2021-01-08 9:31
 * @Author XuX
 */
public class T08_regexTest {
    public static void main(String[] args) {
        boolean bRes1 = FStringUtils.regex("\\w*", "");
        boolean bRes2 = FStringUtils.regex("\\w*", null);
        boolean bRes3 = FStringUtils.regex("\\w*", "hello1+3");
        boolean bRes4 = FStringUtils.regex("\\w+", "");
        boolean bRes5 = FStringUtils.regex("\\w+", null);
        boolean bRes6 = FStringUtils.regex("\\w+", "hello1+3");
        System.out.println(bRes1);  // true
        System.out.println(bRes2);  // false
        System.out.println(bRes3);  // false
        System.out.println(bRes4);  // false
        System.out.println(bRes5);  // false
        System.out.println(bRes6);  // false
    }
}
