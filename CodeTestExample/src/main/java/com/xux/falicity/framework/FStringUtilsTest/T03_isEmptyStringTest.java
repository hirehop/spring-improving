package com.xux.falicity.framework.FStringUtilsTest;

import com.xux.falicity.framework.util.FStringUtils;

/**
 * @Date 2021-01-07 21:15
 * @Author XuX
 */
public class T03_isEmptyStringTest {
    public static void main(String[] args) {
        System.out.println(FStringUtils.isEmptyString(""));  // true
        System.out.println(FStringUtils.isEmptyString(null));  // true
        System.out.println(FStringUtils.isEmptyString("hello"));  // false

        System.out.println(FStringUtils.haveEmptyString("", "hello", "world"));  // true
        System.out.println(FStringUtils.haveEmptyString(null, "hello", "world"));  // true
        System.out.println(FStringUtils.haveEmptyString("java", "hello", "world"));  // false

        System.out.println(FStringUtils.allEmptyString("", "hello", "world"));  // false
        System.out.println(FStringUtils.allEmptyString(null, "hello", "world"));  // false
        System.out.println(FStringUtils.allEmptyString("java", "hello", "world"));  // true
    }
}
