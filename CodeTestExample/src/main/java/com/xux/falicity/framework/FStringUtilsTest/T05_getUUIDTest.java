package com.xux.falicity.framework.FStringUtilsTest;

import com.xux.falicity.framework.util.FStringUtils;

/**
 * @Date 2021-01-07 22:24
 * @Author XuX
 */
public class T05_getUUIDTest {
    public static void main(String[] args) {
        for (int i = 0; i < 100; i++) {
            String id = FStringUtils.getUUID();
            System.out.println(id);
        }
    }
}
