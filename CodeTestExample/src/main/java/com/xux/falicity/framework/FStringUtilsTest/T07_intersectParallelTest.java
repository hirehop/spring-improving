package com.xux.falicity.framework.FStringUtilsTest;

import com.xux.falicity.framework.util.FStringUtils;

import java.util.Arrays;

/**
 * @Date 2021-01-08 8:46
 * @Author XuX
 */
public class T07_intersectParallelTest {
    public static void main(String[] args) {
        String sName1 = "hello";
        String sName2 = "world";
        String[] saName1 = {"aaa", "bbb"};
        String[] saName2 = {"ccc", "bbb"};
        // intersect 相交、交叉
        // parallel 平行
        // union 交
        String[] intersect = FStringUtils.intersect(saName1, saName2);
        String[] parallel = FStringUtils.parallel(saName1, saName2);
        String[] union = FStringUtils.union(saName1, saName2);
        System.out.println(Arrays.toString(intersect));  // [bbb]
        System.out.println(Arrays.toString(parallel));  // [aaa, ccc]
        System.out.println(Arrays.toString(union));  // [aaa, ccc, bbb]
    }
}
