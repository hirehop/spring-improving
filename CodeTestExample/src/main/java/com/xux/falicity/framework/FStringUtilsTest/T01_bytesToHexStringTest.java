package com.xux.falicity.framework.FStringUtilsTest;

import com.xux.falicity.framework.util.FStringUtils;

import java.nio.charset.StandardCharsets;

/**
 * @Date 2021-01-07 20:17
 * @Author XuX
 */
public class T01_bytesToHexStringTest {
    public static void main(String[] args) {
        String sName = "hello,World";
        String sNameNew = FStringUtils.bytesToHexString(sName.getBytes(StandardCharsets.UTF_8));
        System.out.println(sNameNew);  // 68656C6C6F2C576F726C64
        byte[] bytes = FStringUtils.hexStringToBytes(sNameNew);
        System.out.println(new String(bytes, StandardCharsets.UTF_8));  // hello,World
    }
}
