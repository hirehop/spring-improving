package com.xux.falicity.framework.FStringUtilsTest;

import com.xux.falicity.framework.util.FStringUtils;

/**
 * @Date 2021-01-07 22:14
 * @Author XuX
 */
public class T04_countSubstringTest {
    public static void main(String[] args) {
        String sName = "hello,aabbccccdddcc";
        int count = FStringUtils.countSubstring(sName, "cc");  // 3
        System.out.println(count);

    }
}
