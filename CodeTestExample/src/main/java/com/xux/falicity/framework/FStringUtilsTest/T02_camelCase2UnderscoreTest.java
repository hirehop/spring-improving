package com.xux.falicity.framework.FStringUtilsTest;

import com.xux.falicity.framework.util.FStringUtils;

/**
 * @Date 2021-01-07 21:06
 * @Author XuX
 */
public class T02_camelCase2UnderscoreTest {
    public static void main(String[] args) {
        String sName = "helloWorldJava";
        String sNameUnderScore1 = FStringUtils.camelCase2Underscore(sName, 0);
        String sNameUnderScore2 = FStringUtils.camelCase2Underscore(sName, 1);
        System.out.println(sNameUnderScore1);  // hello_world_java
        System.out.println(sNameUnderScore2);  // Hello_world_java

        String sNameCamelCase1 = FStringUtils.underScore2CamelCase(sNameUnderScore1, 0);
        String sNameCamelCase2 = FStringUtils.underScore2CamelCase(sNameUnderScore1, 1);
        System.out.println(sNameCamelCase1);  // helloWorldJava
        System.out.println(sNameCamelCase2);  // HelloWorldJava
        String sNameCamelCase3 = FStringUtils.underScore2CamelCase(sNameUnderScore2, 0);
        String sNameCamelCase4 = FStringUtils.underScore2CamelCase(sNameUnderScore2, 1);
        System.out.println(sNameCamelCase3);  // helloWorldJava
        System.out.println(sNameCamelCase4);  // HelloWorldJava
    }
}
