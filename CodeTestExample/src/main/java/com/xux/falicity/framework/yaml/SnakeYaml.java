package com.xux.falicity.framework.yaml;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.yaml.snakeyaml.Yaml;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Map;

/**
 * @Date 2021-01-08 17:10
 * @Author XuX
 */
public class SnakeYaml {
    private static Logger logger = LoggerFactory.getLogger(SnakeYaml.class);

    @Test
    public void testSwitchMap() {
        InputStream inputStream = null;
        try {
            Yaml yaml = new Yaml();
            inputStream = new FileInputStream("src\\main\\resources\\application.yaml");

            // LinkedHashMap
            // ArrayList
            // Integer
            // Boolean
            // Double
            // String
            Map map = yaml.loadAs(inputStream, Map.class);

            System.out.println(map);
        } catch (IOException e) {
            logger.error(e.toString());
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
