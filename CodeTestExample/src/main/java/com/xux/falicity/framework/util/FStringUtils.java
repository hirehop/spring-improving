package com.xux.falicity.framework.util;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * JDK1.8 Required!
 * @Date 2021-01-07 20:52
 * @Author XuX
 */
public class FStringUtils {

    private static final Pattern UP_PATTERN = Pattern.compile("[A-Z]");

    /**
     * 字节数组转十六进制字符串
     *
     * @param bArray 字节数组
     * @return hexString 十六进制字符串
     */
    public static String bytesToHexString(byte[] bArray) {
        StringBuffer sb = new StringBuffer(bArray.length);
        for (int i = 0; i < bArray.length; ++i) {
            String sTemp = Integer.toHexString(255 & bArray[i]);
            if (sTemp.length() < 2) {
                sb.append(0);
            }
            sb.append(sTemp.toUpperCase());
        }
        return sb.toString();
    }

    /**
     * 十六进制字符串转字节数组
     *
     * @param hex
     * @return
     */
    public static byte[] hexStringToBytes(String hex) {
        int len = hex.length() / 2;
        byte[] result = new byte[len];
        char[] chars = hex.toCharArray();
        for (int i = 0; i < len; ++i) {
            int pos = i * 2;
            result[i] = (byte) (toByte(chars[pos]) << 4 | toByte(chars[pos + 1]));
        }
        return result;
    }

    private static int toByte(char c) {
        byte b = (byte) "0123456789ABCDEF".indexOf(c);
        return b;
    }

    /**
     * camelCase转underScore，参数t控制首字母是否大写
     *
     * @param param 待转换字符串
     * @param t     非0表示首字母需要大写
     * @return
     */
    public static String camelCase2Underscore(String param, int t) {
        if (param != null && !param.equals("")) {
            StringBuffer buffer = new StringBuffer(param);
            Matcher mc = UP_PATTERN.matcher(param);
            for (int i = 0; mc.find(); ++i) {
                buffer.replace(mc.start() + i, mc.end() + i, "_" + mc.group().toLowerCase());
            }
            if ('_' == buffer.charAt(0)) {
                buffer.deleteCharAt(0);
            }
            if (t == 0) {
                String head = String.valueOf(buffer.charAt(0)).toLowerCase();
                buffer.setCharAt(0, head.charAt(0));
            } else {
                String head = String.valueOf(buffer.charAt(0)).toUpperCase();
                buffer.setCharAt(0, head.charAt(0));
            }
            return buffer.toString();
        } else {
            return "";
        }
    }

    /**
     * underScore转camelCase，参数t限制首字母是否大写
     *
     * @param str 待转换字符串
     * @param t   非0表示首字母需要大写
     * @return
     */
    public static String underScore2CamelCase(String str, int t) {
        str = str.toLowerCase();
        String[] names = str.split("_");
        StringBuffer buffer = new StringBuffer();
        for (int i = 0; i < names.length; ++i) {
            char[] ch = names[i].toCharArray();
            if (t == 0 && i == 0) {
                buffer.append(ch);
            } else {
                if (ch[0] >= 'a' && ch[0] <= 'z') {
                    ch[0] = (char) (ch[0] - 32);
                }
                buffer.append(ch);
            }
        }
        return buffer.toString();
    }

    public static boolean isEmptyString(String s) {
        return s == null || "".equals(s);
    }

    public static boolean haveEmptyString(String... s) {
        String[] var4 = s;
        int var3 = s.length;
        for (int var2 = 0; var2 < var3; ++var2) {
            String string = var4[var2];
            if (isEmptyString(string)) {
                return true;
            }
        }
        return false;
    }

    public static boolean allEmptyString(String... s) {
        String[] var5 = s;
        int var3 = s.length;
        for (int var2 = 0; var2 < var3; ++var2) {
            String string = var5[var2];
            if (isEmptyString(string)) {
                return false;
            }
        }
        return true;
    }

    /**
     * 计算子串出现次数（非贪婪匹配）
     *
     * @param s
     * @param c
     * @return
     */
    public static int countSubstring(String s, String c) {
        boolean bool = true;
        int startIndex = 0;
        int count = 0;
        int lastIndex = 0;
        while (true) {
            while (bool) {
                int index = s.indexOf(c, startIndex);
                if (index > -1) {
                    lastIndex = index;
                    startIndex = index + c.length();
                    ++count;
                } else if (s.lastIndexOf(c) < 0 || lastIndex == s.lastIndexOf(c)) {
                    bool = false;
                }
            }
            return count;
        }
    }

    /**
     * UUID原格式：4bc7e986-d08a-4d1e-879c-db1b0637fc00
     * 现将"-"替换为""
     *
     * @return 32位随机字符串
     */
    public static String getUUID() {
        return UUID.randomUUID().toString().replaceAll("-", "");
    }

    /**
     * 查看文件字符集
     *
     * @param sourceFile File对象
     * @return 字符集String
     */
    public static String getFileCharset(File sourceFile) {
        String charset = "GBK";
        byte[] first3Bytes = new byte[3];
        try {
            boolean checked = false;
            BufferedInputStream bis = new BufferedInputStream(new FileInputStream(sourceFile));
            bis.mark(0);
            int read = bis.read(first3Bytes, 0, 3);
            if (read == -1) {
                return charset;
            }
            if (first3Bytes[0] == -1 && first3Bytes[1] == -2) {
                charset = "UTF-16LE";
                checked = true;
            } else if (first3Bytes[0] == -2 && first3Bytes[1] == -1) {
                charset = "UTF-16BE";
                checked = true;
            } else if (first3Bytes[0] == -17 && first3Bytes[1] == -69 && first3Bytes[2] == -65) {
                charset = "UTF-8";
                checked = true;
            }
            bis.reset();
            if (!checked) {
                label73:
                do {
                    do {
                        if ((read = bis.read()) == -1 || read >= 240 || 128 <= read && read <= 191) {
                            break label73;
                        }
                        if (192 <= read && read <= 223) {
                            read = bis.read();
                            continue label73;
                        }
                    } while (224 > read || read > 239);

                    read = bis.read();
                    if (128 <= read && read <= 191) {
                        read = bis.read();
                        if (128 <= read && read <= 191) {
                            charset = "UTF-8";
                        }
                    }
                    break;
                } while (128 <= read && read <= 191);
            }
            bis.close();
        } catch (Exception var6) {
            var6.printStackTrace();
        }
        return charset;
    }

    public static String[] parallel(String[] arr1, String[] arr2) {
        List<String> list1 = new ArrayList();
        List<String> list2 = new ArrayList();
        String[] longerArr = arr1;
        String[] shortArr = arr2;
        if (arr1.length < arr2.length) {
            longerArr = arr2;
            shortArr = arr1;
        }

        String[] var9 = longerArr;
        int var8 = longerArr.length;

        String string;
        int var7;
        for (var7 = 0; var7 < var8; ++var7) {
            string = var9[var7];
            if (!list1.contains(string)) {
                list1.add(string);
            }
        }

        var9 = shortArr;
        var8 = shortArr.length;

        for (var7 = 0; var7 < var8; ++var7) {
            string = var9[var7];
            if (list1.contains(string)) {
                list1.remove(string);
                list2.add(string);
            } else if (!list2.contains(string)) {
                list1.add(string);
            }
        }

        String[] result = new String[0];
        return (String[]) list1.toArray(result);
    }

    public static String[] intersect(String[] arr1, String[] arr2) {
        Map<String, Boolean> map = new HashMap();
        List<String> list = new ArrayList();
        String[] var7 = arr1;
        int var6 = arr1.length;

        String string;
        int var5;
        for (var5 = 0; var5 < var6; ++var5) {
            string = var7[var5];
            if (!map.containsKey(string)) {
                map.put(string, false);
            }
        }

        var7 = arr2;
        var6 = arr2.length;

        for (var5 = 0; var5 < var6; ++var5) {
            string = var7[var5];
            if (map.containsKey(string)) {
                map.put(string, true);
            }
        }

        Iterator var10 = map.entrySet().iterator();

        while (var10.hasNext()) {
            Map.Entry<String, Boolean> e = (Map.Entry) var10.next();
            if ((Boolean) e.getValue()) {
                list.add((String) e.getKey());
            }
        }

        String[] result = new String[0];
        return (String[]) list.toArray(result);
    }

    public static String[] union(String[] arr1, String[] arr2) {
        Set<String> set = new HashSet();
        String[] var6 = arr1;
        int var5 = arr1.length;

        String string;
        int var4;
        for (var4 = 0; var4 < var5; ++var4) {
            string = var6[var4];
            set.add(string);
        }

        var6 = arr2;
        var5 = arr2.length;

        for (var4 = 0; var4 < var5; ++var4) {
            string = var6[var4];
            set.add(string);
        }

        String[] result = new String[0];
        return (String[]) set.toArray(result);
    }

    /**
     * 正则表达式匹配字符串
     *
     * @param regex 正则表达式
     * @param value 待匹配字符串
     * @return
     */
    public static boolean regex(String regex, String value) {
        if (value == null) {
            return false;
        } else {
            Pattern p = Pattern.compile(regex);
            Matcher m = p.matcher(value);
            return m.matches();
        }
    }

    private static final String[] solidStringN, solidStringC1, solidStringC2;

    static {
        solidStringN = "0123456789".split("");
        solidStringC1 = "abcdefghijklmnopqrstuvwxyz".split("");
        solidStringC2 = "ABCDEFGHIJKLMNOPQRSTUVWXYZ".split("");
    }

    /**
     * 支持Regex表达式如下：{"\\d", "\\w", "[0-9]", "[a-z]", "[A-Z]", "[a-zA-Z]", "[0-9a-z]", "[0-9A-Z]", "[0-9a-zA-Z]"}
     *
     * @param regex  正则表达式
     * @param length 待生成字符串长度
     * @return
     */
    public static String getStringFromRegex(String regex, int length) {
        String res = "";
        switch (regex) {
            case "[0-9a-zA-Z]":
                res = getStringNC1C2(length);
                break;
            case "\\d":
            case "[0-9]":
                res = getStringN(length);
                break;
            case "\\w":
                res = getStringNC1C2W(length);
                break;
            case "[a-z]":
                res = getStringC1(length);
                break;
            case "[A-Z]":
                res = getStringC2(length);
                break;
            case "[0-9a-z]":
                res = getStringNC1(length);
                break;
            case "[0-9A-Z]":
                res = getStringNC2(length);
                break;
            case "[a-zA-Z]":
                res = getStringC1C2(length);
                break;
            default:
                throw new RuntimeException("该正则表达式暂不支持。");
        }


        return res;
    }

    private static String getStringN(int N) {
        StringBuilder sb = new StringBuilder();
        List<String> tempFullList = new ArrayList<>();
        tempFullList.addAll(Arrays.asList(solidStringN));
        Collections.shuffle(tempFullList);
        int length = tempFullList.size();
        for (int i = 0; i < N; i++) {
            int index = (int) (Math.random() * (length));
            sb.append(tempFullList.get(index));
        }
        return sb.toString();
    }

    private static String getStringNC1C2(int N) {
        StringBuilder sb = new StringBuilder();
        List<String> tempFullList = new ArrayList<>();
        tempFullList.addAll(Arrays.asList(solidStringN));
        tempFullList.addAll(Arrays.asList(solidStringC1));
        tempFullList.addAll(Arrays.asList(solidStringC2));
        Collections.shuffle(tempFullList);
        int length = tempFullList.size();
        for (int i = 0; i < N; i++) {
            int index = (int) (Math.random() * (length));
            sb.append(tempFullList.get(index));
        }
        return sb.toString();
    }

    private static String getStringNC1C2W(int N) {
        StringBuilder sb = new StringBuilder();
        List<String> tempFullList = new ArrayList<>();
        tempFullList.addAll(Arrays.asList(solidStringN));
        tempFullList.addAll(Arrays.asList(solidStringC1));
        tempFullList.addAll(Arrays.asList(solidStringC2));
        tempFullList.add("_");
        Collections.shuffle(tempFullList);
        int length = tempFullList.size();
        for (int i = 0; i < N; i++) {
            int index = (int) (Math.random() * (length));
            sb.append(tempFullList.get(index));
        }
        return sb.toString();
    }

    private static String getStringC1(int N) {
        StringBuilder sb = new StringBuilder();
        List<String> tempFullList = new ArrayList<>();
        tempFullList.addAll(Arrays.asList(solidStringC1));
        Collections.shuffle(tempFullList);
        int length = tempFullList.size();
        for (int i = 0; i < N; i++) {
            int index = (int) (Math.random() * (length));
            sb.append(tempFullList.get(index));
        }
        return sb.toString();
    }

    private static String getStringC2(int N) {
        return getStringC1(N).toUpperCase();
    }

    private static String getStringC1C2(int N) {
        StringBuilder sb = new StringBuilder();
        List<String> tempFullList = new ArrayList<>();
        tempFullList.addAll(Arrays.asList(solidStringC1));
        tempFullList.addAll(Arrays.asList(solidStringC2));
        Collections.shuffle(tempFullList);
        int length = tempFullList.size();
        for (int i = 0; i < N; i++) {
            int index = (int) (Math.random() * (length));
            sb.append(tempFullList.get(index));
        }
        return sb.toString();
    }

    private static String getStringNC1(int N) {
        if (N == 32) {
            // JDK1.8
            return String.join("", UUID.randomUUID().toString().split("-"));
        } else {
            StringBuilder sb = new StringBuilder();
            List<String> tempFullList = new ArrayList<>();
            tempFullList.addAll(Arrays.asList(solidStringN));
            tempFullList.addAll(Arrays.asList(solidStringC1));
            tempFullList.addAll(Arrays.asList(solidStringC2));
            Collections.shuffle(tempFullList);
            int length = tempFullList.size();
            for (int i = 0; i < N; i++) {
                int index = (int) (Math.random() * (length));
                sb.append(tempFullList.get(index));
            }
            return sb.toString();
        }
    }

    private static String getStringNC2(int N) {
        return getStringNC1(N).toUpperCase();
    }
}
