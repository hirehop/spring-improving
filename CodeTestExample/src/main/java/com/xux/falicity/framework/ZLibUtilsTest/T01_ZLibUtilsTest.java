package com.xux.falicity.framework.ZLibUtilsTest;

import com.xux.falicity.framework.util.ZLibUtils;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;

/**
 * @Date 2021-01-07 19:31
 * @Author XuX
 */
public class T01_ZLibUtilsTest {
        public static void main(String[] args) throws IOException {
        DateTimeFormatter formatForFileTimeStamp = DateTimeFormat.forPattern("yyyy_MM_dd_HH_mm_ss");

        String path = System.getProperty("java.io.tmpdir") + "test_"+ new DateTime().toString(formatForFileTimeStamp) +".tmp";
        File tempFile = new File(path);
        if(!tempFile.exists()){
            System.out.println(tempFile.createNewFile());
        }
        // 压缩byte[]数组
        String sName = "hello,world";
        byte[] bytes1 = sName.getBytes();
        OutputStream os = new BufferedOutputStream(new FileOutputStream(path));
        ZLibUtils.compress(bytes1,os);

        BufferedInputStream is = new BufferedInputStream(new FileInputStream(path));
        byte[] byte2 = ZLibUtils.decompress(is);
        System.out.println(Arrays.toString(byte2));
        System.out.println(new String(byte2, StandardCharsets.UTF_8));

        os.close();
        is.close();

        System.out.println(tempFile.delete());
    }
}
