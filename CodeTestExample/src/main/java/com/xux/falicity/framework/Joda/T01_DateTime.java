package com.xux.falicity.framework.Joda;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.util.Date;

/**
 * @Date 2021-01-08 14:50
 * @Author XuX
 */
public class T01_DateTime {
    public static void main(String[] args) {
        // 获取当前时间
        DateTime dateTime = new DateTime();
        System.out.println(dateTime);  // 2021-01-08T14:57:28.944+08:00

        // 时间格式化
        DateTimeFormatter format = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss.SSS");
        String sDateTime = dateTime.toString(format);
        System.out.println(sDateTime);  // 2021-01-08 15:08:48.223

        // 解析时间字符串为DateTime
        DateTime dateTime1 = DateTime.parse(sDateTime, format);
        System.out.println(dateTime1);  // 2021-01-08T15:08:48.223+08:00

        // DateTime与Date的转换
        // Date也是存有毫秒数的.SSS,只是在toString中没有将毫秒输出
        Date date = dateTime.toDate();
        System.out.println(date);  // Fri Jan 08 15:08:48 CST 2021
        DateTime dateTime2 = new DateTime(date);
        System.out.println(dateTime2);  // 2021-01-08T15:08:48.223+08:00


        // 可以操作年月日时分秒
        DateTime dateTime3 = dateTime2.plusMillis(1);
        System.out.println(dateTime3);  // 2021-01-08T15:08:48.224 +08:00

        // 获取当月月初和月末
        DateTime dtEarlyMonth = dateTime3.plusDays(-dateTime3.getDayOfMonth() + 1);
        System.out.println(dtEarlyMonth);  // 2021-01-01T15:53:35.174+08:00
        DateTime dtEndMonth = dateTime3.plusMonths(1).plusDays(-dateTime3.getDayOfMonth());
        System.out.println(dtEndMonth);  // 2021-01-31T15:53:35.174+08:00
    }
}
