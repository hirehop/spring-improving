package com.xux.falicity.controller;

import com.xux.falicity.entity.User;
import com.xux.falicity.framework.util.FStringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import java.io.IOException;
import java.io.Writer;

/**
 * Description
 *
 * @Create by X
 * @Date 2021/2/4 22:14
 */
@Controller
public class Hello2Controller {
    private User user;

    public Hello2Controller(){};

    @Autowired
    public void setUser(User user){
        this.user = user;
    }

    @RequestMapping("/testAutowired")
    public void testAutowired(Writer out) throws IOException {
        user.setId(FStringUtils.getUUID());
//        user.setUsername("Jack");
//        user.setPassword("ABCD#1234");
//        user.setGender("男");
        out.write(user.toString());
        System.out.println(new User());
    }
}
