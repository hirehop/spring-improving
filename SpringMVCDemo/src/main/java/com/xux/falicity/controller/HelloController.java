package com.xux.falicity.controller;

import com.xux.falicity.entity.User;
import com.xux.falicity.framework.util.FStringUtils;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.Writer;
import java.util.Map;

/**
 * @Date 2021-01-10 19:36
 * @Author XuX
 */
@Controller
public class HelloController {

    @ModelAttribute
    public void pageload(@RequestParam(value = "id", required = false) String id, Map<String, Object> map) {
        if (!FStringUtils.isEmptyString(id)) {
            User user = new User("anonymous", "anonymous", "anonymous", "male");
            map.put("user", user);
        }
    }

    @RequestMapping("/hello")
    public ModelAndView index() {
        ModelAndView mv = new ModelAndView();
        mv.setViewName("hello");
        return mv;
    }

    @RequestMapping("/page1")
    public void function1(HttpServletRequest request, HttpServletResponse response,
                          Writer out) throws IOException {
        // /page1
        String servletPath = request.getServletPath();
        // /SpringMVCDemo
        String servletContext = request.getContextPath();
        out.write(servletPath + "," + servletContext);
    }

    @RequestMapping("/page2")
    public String function2(Map<String, Object> map) {
        map.put("info", new DateTime().toString(DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss")));
        return "page2";
    }

    @RequestMapping("/testModelAttribute")
    public String function3(User user) {
        System.out.println(user);
        return "hello";
    }
}
