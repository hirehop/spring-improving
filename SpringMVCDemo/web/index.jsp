<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%pageContext.setAttribute("ctx", request.getContextPath());%>
<html>
<head>
    <title>欢迎</title>
    <script src="${ctx}/commons/jquery/jquery-3.5.1.js"></script>
    <script>
        /* EL表达式${ctx}会在jsp解析时转换为字符串，直接将EL表达式放在字符串中即可，不需要使用+进行拼接*/
        $(function () {
            $.ajax({
                type: "POST",
                url: "${ctx}/page1",
                async: true,
                dataType: "TEXT",
                success: function (data) {
                    console.log(data);
                    console.log(typeof (data));
                },
                error: function (data) {
                    console.log("ERROR: " + data);
                }
            })
        })
    </script>
</head>
<body>
<h2>首页</h2>
<br><br>
<a href="${ctx}/page2">Page2</a>
<br><br>
<form action="${ctx}/testModelAttribute" method="post">
    <input type="hidden" name="id" value="179074001"/>
    username:<input type="text" name="username" value="Tom"/>
    <br>
    gender:<input type="text" name="gender" value="male"/>
    <br>
    <input type="submit" value="Submit"/>
</form>
<br><br>
</body>
</html>
