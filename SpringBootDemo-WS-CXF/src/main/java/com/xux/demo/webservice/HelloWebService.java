package com.xux.demo.webservice;

import javax.jws.WebService;

/**
 * @author X
 * @date 2021-02-25 16:46
 */
@WebService
public interface HelloWebService {
    String hello(String info);
    String getRealData(String jsonData);
}
