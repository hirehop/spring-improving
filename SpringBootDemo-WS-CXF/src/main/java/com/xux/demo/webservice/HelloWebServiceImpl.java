package com.xux.demo.webservice;

import org.activiti.engine.impl.util.json.JSONArray;
import org.activiti.engine.impl.util.json.JSONObject;
import org.springframework.stereotype.Service;

import javax.jws.WebService;
import java.util.HashMap;
import java.util.Map;

/**
 * @author X
 * @date 2021-02-25 16:47
 */
@WebService
@Service
public class HelloWebServiceImpl implements HelloWebService {
    @Override
    public String hello(String info) {
        return "hello "+info;
    }

    /**
     * 根据json数据模拟查询响应结果返回
     * @param jsonData
     * @return
     */
    @Override
    public String getRealData(String jsonData) {
        JSONArray jsonArray = new JSONArray(jsonData);
        String[] tags = new String[jsonArray.length()];
        for(int i=0;i<jsonArray.length();i++){
            tags[i] = (String) jsonArray.get(i);
        }

        Map<String, String> resMap = new HashMap<>();
        for(String tag : tags){
            resMap.put(tag, tag+"_value");
        }

        return new JSONObject(resMap).toString();
    }
}
