package com.xux.demo.webservice.config;

import com.xux.demo.webservice.HelloWebService;
import org.apache.cxf.Bus;
import org.apache.cxf.jaxws.EndpointImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.xml.ws.Endpoint;

@Configuration
public class CXFConfig {

    @Autowired
    private Bus bus;
    @Autowired
    private HelloWebService helloWebService;

    /**
     * 发布服务
     * 指定访问url => http://localhost:8091/services/hello?wsdl
     * @return
     */
    @Bean
    public Endpoint userEndpoint(){
        EndpointImpl endpoint = new EndpointImpl(bus,helloWebService);
        endpoint.publish("/hello");
        return endpoint;
    }
}
