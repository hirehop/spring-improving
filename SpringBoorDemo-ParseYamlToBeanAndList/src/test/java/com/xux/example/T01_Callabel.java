package com.xux.example;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.FutureTask;
import java.util.concurrent.TimeUnit;

public class T01_Callabel {
    public static void main(String[] args) {
        try {
            testThread3();
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }
    }

    private static void testThread3() throws InterruptedException, ExecutionException {
        Callable<String> myCallable = () -> {
            System.out.println("start");
            TimeUnit.SECONDS.sleep(1);
            return "end";
        };
        FutureTask<String> task = new FutureTask<>(myCallable);
        new Thread(task).start();
        // 阻塞，直至获得结果
        System.out.println(task.get());


        for(int i=0;i<100;i++){
            System.out.println("???");
            System.out.println("???");
        }


        // TimeUnit枚举类调用sleep底层还是调用了当前线程的sleep方法
        TimeUnit.SECONDS.sleep(1);

        System.out.println(task.get());
    }
}
