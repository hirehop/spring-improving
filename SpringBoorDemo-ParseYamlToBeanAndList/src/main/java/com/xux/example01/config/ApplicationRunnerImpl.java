package com.xux.example01.config;

import com.xux.example01.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

@Component
public class ApplicationRunnerImpl implements ApplicationRunner {
    @Autowired
    private ServerConfig servers;

    @Autowired
    private User user;

    @Override
    public void run(ApplicationArguments args) {
        System.out.println(servers);
        System.out.println(user);
    }
}
