package com.xux.example01.config;

import com.xux.example01.entity.Server;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * 读取yml配置文件 => List
 * 要求：
 *    - 双层数据，外层标记在@ConfigurationProperties注解的prefix参数中
 *    - List属性名称和yaml第二层属性名保持一致
 */
@Component
@ConfigurationProperties(prefix = "db")
@Data
public class ServerConfig {
    private List<Server> servers = new ArrayList<>();
}

