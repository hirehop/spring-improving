package com.xux.example01.listener;

import com.xux.example01.config.ServerConfig;
import com.xux.example01.entity.User;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextClosedEvent;
import org.springframework.stereotype.Component;

@Component
public class ApplicationEventListener implements ApplicationListener {

    @Autowired
    private User user;

    @Autowired
    private ServerConfig servers;

    @SneakyThrows
    @Override
    public void onApplicationEvent(ApplicationEvent event) {
        if(event instanceof ApplicationReadyEvent){
            System.out.println(user);
            System.out.println(servers);
            System.out.println("应用启动完成");
        }else if(event instanceof ContextClosedEvent){
            System.out.println("应用关闭");
        }
    }
}
