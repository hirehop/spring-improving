package com.xux.example01.entity;

import lombok.Data;

@Data
public class Server {
    private String name;
    private String port;
    private String address;
}
