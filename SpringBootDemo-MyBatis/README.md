# SpringBoot集成MyBatis

---

### 一：jdbc与连接池

```xml
<dependency>
    <groupId>org.mybatis.spring.boot</groupId>
    <artifactId>mybatis-spring-boot-starter</artifactId>
    <version>2.1.4</version>
</dependency>

// 上述依赖会间接注入org.springframework.boot:spring-boot-starter-jdbc:2.1.4
// org.springframework.boot:spring-boot-starter-jdbc:2.1.4 会间接注入 org.springframework.boot:spring-boot-starter:2.4.2
// org.springframework.boot:spring-boot-starter-jdbc:2.1.4 会间接注入 com.zaxxer:HikariCP:3.4.5
// org.springframework.boot:spring-boot-starter-jdbc:2.1.4 会间接注入 org.springframework:spring-jdbc:5.3.3
```



### 二：mysql数据库连接库

```xml
<dependency>
    <groupId>mysql</groupId>
    <artifactId>mysql-connector-java</artifactId>
    <version>8.0.23</version>
</dependency>
```



### 三：Mapper

```java
public interface StudentMapper {

    @Select("SELECT * FROM STUDENT WHERE ID=#{id}")
    Student get(Integer id);

    @Insert("INSERT INTO STUDENT VALUES(#{id},#{name},#{gender},#{age})")
    void insert(Student student);
}
```



### 四：注入

```java
private StudentMapper studentMapper;
@Autowired
public void setStudentMapper(StudentMapper studentMapper) {
    this.studentMapper = studentMapper;
}
```















