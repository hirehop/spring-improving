package com.xux.demo.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Description
 *
 * @Create by X
 * @Date 2021/2/7 22:39
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class State {
    private byte state;
}
