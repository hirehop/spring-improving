package com.xux.demo.mappers;

import com.xux.demo.entity.Student;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Component;

/**
 * Description
 *
 * @Create by X
 * @Date 2021/2/6 16:37
 */
@Mapper
public interface StudentMapper {

    @Select("SELECT * FROM STUDENT WHERE ID=#{id}")
    Student get(Integer id);

    @Insert("INSERT INTO STUDENT VALUES(#{id},#{name},#{gender},#{age})")
    void insert(Student student);
}
