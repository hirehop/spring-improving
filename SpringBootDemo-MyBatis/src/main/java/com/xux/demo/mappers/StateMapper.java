package com.xux.demo.mappers;

import com.xux.demo.entity.State;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Description
 *
 * @Create by X
 * @Date 2021/2/7 22:55
 */
@Mapper
public interface StateMapper {

    @Select("SELECT * FROM STATE")
    List<State> getAll();

    @Insert("INSERT INTO STATE VALUES(#{state})")
    void insert(State state);
}
