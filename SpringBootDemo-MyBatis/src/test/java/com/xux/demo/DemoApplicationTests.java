package com.xux.demo;

import com.xux.demo.entity.State;
import com.xux.demo.entity.Student;
import com.xux.demo.mappers.StateMapper;
import com.xux.demo.mappers.StudentMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.sql.SQLException;
import java.util.List;

@SpringBootTest
class DemoApplicationTests {

    private StudentMapper studentMapper;
    private StateMapper stateMapper;

    @Autowired
    public void setStateMapper(StateMapper stateMapper) {
        this.stateMapper = stateMapper;
    }

    @Autowired
    public void setStudentMapper(StudentMapper studentMapper) {
        this.studentMapper = studentMapper;
    }

    @Test
    void contextLoads1() {
        List<State> all = stateMapper.getAll();
        all.forEach(System.out::println);
    }

    @Test
    void contextLoads() {
        System.out.println(studentMapper.get(10006));
//        Student student = new Student(99999,"error",0,999);
//        studentMapper.insert(student);
    }

}
